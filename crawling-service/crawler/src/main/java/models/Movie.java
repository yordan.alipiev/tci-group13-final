package models;

import java.util.ArrayList;

/**
 * A movie object containing information about the {@link Movie}, its
 * directors and stars.
 */
public class Movie {

    /**
     * The name of the movie.
     */
    private String name;

    /**
     * The genre of the movie.
     */
    private String genre;

    /**
     * The format in which the movie is stored.
     */
    private String format;

    /**
     * The year in which the movie was officially released.
     */
    private String year;

    /**
     * The leading director of the movie.
     */
    private String director;

    /**
     * The names of the main screen writers of the movie.
     */
    private ArrayList<String> writers;

    /**
     * The names of the actors with the leading roles in the movie.
     */
    private ArrayList<String> stars;

    /**
     * Creates an empty movie object, initializing its list of stars
     * and writers.
     */
    public Movie() {
        writers = new ArrayList<>();
        stars = new ArrayList<>();
    }

    /**
     * Gets the name of the movie.
     *
     * @return The name of the movie.
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the name of the movie.
     *
     * @param name The name of the movie to be set.
     * @throws IllegalArgumentException in case the provided value was
     * null or empty.
     */
    public void setName(String name) throws IllegalArgumentException {
        this.name = validateInput(name);
    }

    /**
     * Gets the genre of the movie.
     *
     * @return The genre of the movie.
     */
    public String getGenre() {
        return genre;
    }

    /**
     * Sets the genre of the movie.
     *
     * @param genre The genre of the movie to be set.
     * @throws IllegalArgumentException in case the provided value was
     * null or empty.
     */
    public void setGenre(String genre) throws IllegalArgumentException {
        this.genre = validateInput(genre);
    }

    /**
     * Gets the format of the movie.
     *
     * @return The format of the movie.
     */
    public String getFormat() {
        return format;
    }

    /**
     * Sets the format of the movie.
     *
     * @param format The format of the movie to be set.
     * @throws IllegalArgumentException in case the provided value was
     * null or empty.
     */
    public void setFormat(String format) throws IllegalArgumentException {
        this.format = validateInput(format);
    }

    /**
     * Gets the release year of the movie.
     *
     * @return The release year of the movie.
     */
    public String getYear() {
        return year;
    }

    /**
     * Sets the release year of the movie.
     *
     * @param year The release year of the movie to be set.
     * @throws IllegalArgumentException in case the provided value was null
     * or empty or cannot be parsed to integer.
     */
    public void setYear(String year) throws IllegalArgumentException {
        String yearString = validateInput(year);
        try {
            Integer.parseInt(yearString);
            this.year = yearString;
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException();
        }
    }

    /**
     * Gets the name of the director of the movie.
     *
     * @return The name of the director of the movie.
     */
    public String getDirector() {
        return director;
    }

    /**
     * Sets the name of the director of the movie.
     *
     * @param director The name of the director of the movie to be set.
     * @throws IllegalArgumentException in case the provided value was
     * null or empty.
     */
    public void setDirector(String director) throws IllegalArgumentException {
        this.director = validateInput(director);
    }

    /**
     * Gets a list of the name of the writers of the movie.
     *
     * @return A list of the names of the writers of the movie.
     */
    public ArrayList<String> getWriters() {
        return writers;
    }

    /**
     * Gets a list of the names of the stars in the movie.
     *
     * @return A list of the names of the stars in the movie.
     */
    public ArrayList<String> getStars() {
        return stars;
    }

    /**
     * @return Trimmed representation of the provided string.
     * @throws IllegalArgumentException in case the provided
     * value was null or empty.
     */
    private String validateInput(String input) throws IllegalArgumentException {
        if (input == null || input.trim().isEmpty()) {
            throw new IllegalArgumentException();
        }
        return input.trim();
    }
}
