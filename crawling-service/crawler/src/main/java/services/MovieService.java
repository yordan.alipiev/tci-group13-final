package services;

import java.util.ArrayList;
import java.util.List;

import models.MutableInteger;
import org.jsoup.nodes.Element;

import exceptions.ParsingException;
import models.Movie;
import parsers.MovieParser;

/**
 * MovieService exposing functionality for retrieving movies from
 * {@link Element}.
 */
public class MovieService {

    /**
     * A {@link MovieParser} object responsible for the parsing from
     * {@link Element} to a {@link Movie}.
     */
    private MovieParser movieParser;

    /**
     * Constructs a movie service using a movie parsing object.
     *
     * @param movieParser The movie parser that will be used by the service to
     *                    parse an element into a Movie.
     */
    public MovieService(MovieParser movieParser) {
        this.movieParser = movieParser;
    }

    /**
     * Constructs a movie service.
     */
    public MovieService() {
        this.movieParser = new MovieParser();
    }

    /**
     * Parses each of the elements in the provided list into Movie objects.
     *
     * @param elements The list of elements to be parsed to list of Movies.
     * @param pages The number of pages.
     * @return Returns a collection of {@link Movie} objects.
     * @throws ParsingException Thrown by the parser.
     */
    public List<Movie> getAllMovies(
            List<Element> elements,
            MutableInteger pages)
            throws ParsingException {

        ArrayList<Movie> movies = new ArrayList();

        for (Element element : elements) {
            pages.increment();
            movies.add(movieParser.parse(element));
        }
        return movies;
    }

    /**
     * Parses a given element into a Movie object.
     *
     * @param element The element to be parsed into a Movie object.
     * @return Returns a @link Movie} objects.
     * @throws ParsingException Thrown by the parser.
     */
    public Movie getMovie(Element element) throws ParsingException {
        return movieParser.parse(element);
    }

    /**
     * Searches for a movie with a matching property in the list of
     * provided elements.
     *
     * @param elements The elements which should be searched.
     * @param property The property to look for.
     * @param pages    Reference integer indicating the number of pages
     *                explored.
     * @return Returns a {@link Movie} with a matching property or null,
     * if none is found.
     * @throws ParsingException Thrown by the {@link MovieParser}.
     */
    public Movie searchMovie(
            List<Element> elements,
            String property,
            MutableInteger pages)
            throws ParsingException {
        for (Element e : elements) {
            pages.increment();
            Movie movie = this.movieParser.parseByMatchingProperty(e, property);
            if (movie != null) {
                return movie;
            }
        }

        return null;
    }
}
