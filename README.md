# TCI_Fontys Group 13

## Test coverage crawler : http://yordan.alipiev.gitlab.io/tci-group13-final/crawler_jacoco/index.html

## Test report crawler : http://yordan.alipiev.gitlab.io/tci-group13-final/crawler/index.html

## Test coverage crawler-http : http://yordan.alipiev.gitlab.io/tci-group13-final/crawler-http_jacoco/index.html

## Test report crawler-http : http://yordan.alipiev.gitlab.io/tci-group13-final/crawler-http/index.html

## Check style report crawler: http://yordan.alipiev.gitlab.io/tci-group13-final/crawler_stylecheck/main.html

## Check style report crawler-http: http://yordan.alipiev.gitlab.io/tci-group13-final/crawler-http_stylecheck/main.html

## JMeter report: http://yordan.alipiev.gitlab.io/tci-group13-final/jmeter-report/getDataTest.jmx-20190113-2345.html

## Service deployed at: https://crawling-service.azurewebsites.net/