package parsers;

import java.util.Arrays;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import exceptions.ParsingException;
import models.Movie;

/**
 * MovieParser implements the IGenericParser. Exposes method for parsing a Movie
 * object from {@link Element}.
 */
public class MovieParser implements IGenericParser<Movie> {

    /**
     * Parses the provided element to a {@link Movie} object.
     *
     * @param element The element to parse.
     * @return Returns a parsed Movie.
     * @throws ParsingException in case a property of the movie was null.
     */
    @Override
    public Movie parse(Element element) throws ParsingException {

        Movie movie = new Movie();

        if (!element.className().equals("media-details")) {
            throw new ParsingException();
        } else {
            try {
                String movieName = element.getElementsByTag("h1")
                        .first().text();
                movie.setName(movieName);

                // We can safely assume there is only one <tbody> tag, because
                // of the site structure.
                Element tbody = element.getElementsByTag("tbody").first();
                Elements rows = tbody.select("tr");

                for (Element row : rows) {
                    String headerText = row.getElementsByTag("th")
                            .first().text();

                    switch (headerText) {
                        case "Genre":
                            String genre = row.getElementsByTag("td")
                                    .first().text();
                            movie.setGenre(genre);
                            break;
                        case "Year":
                            String year = row.getElementsByTag("td")
                                    .first().text();
                            movie.setYear(year);
                            break;
                        case "Director":
                            String director = row.getElementsByTag("td")
                                    .first().text();
                            movie.setDirector(director);
                            break;
                        case "Format":
                            String format = row.getElementsByTag("td")
                                    .first().text();
                            movie.setFormat(format);
                            break;
                        case "Writers":
                            String writers = row.getElementsByTag("td")
                                    .first().text();
                            String[] writersArray = writers.split(",");
                            for (int i = 0; i < writersArray.length; i++) {
                                writersArray[i] = writersArray[i].trim();
                            }

                            movie.getWriters()
                                    .addAll(Arrays.asList(writersArray));
                            break;
                        case "Stars":
                            String stars = row.getElementsByTag("td")
                                    .first().text();
                            String[] starsArray = stars.split(",");
                            for (int i = 0; i < starsArray.length; i++) {
                                starsArray[i] = starsArray[i].trim();
                            }
                            movie.getStars().addAll(Arrays.asList(starsArray));
                            break;
                        case "Category":
                            break;
                        default:
                            throw new ParsingException(
                                    "Unknown property of music encountered.");
                    }
                }
            } catch (Exception e) {
                throw new ParsingException(
                        "An expected tag was missing from the element.");
            }
        }
        return movie;
    }

    /**
     * Parses an {@link Element} into a single {@link Movie} object and looks
     * for a property matching the provided one.
     *
     * @param element  The element to parse.
     * @param property The property to look for.
     * @return Returns a music if a matching property is found, else null.
     * @throws ParsingException in case a property of the music was null.
     */
    @Override
    public Movie parseByMatchingProperty(Element element, String property)
            throws ParsingException {
        Movie parsedMovie = this.parse(element);
        property = property.trim();

        if (parsedMovie.getName().contains(property) ||
                parsedMovie.getFormat().contains(property) ||
                parsedMovie.getGenre().contains(property) ||
                parsedMovie.getDirector().contains(property) ||
                parsedMovie.getYear().contains(property) ||
                parsedMovie.getWriters().contains(property) ||
                parsedMovie.getStars().contains(property)) {
            return parsedMovie;
        }

        return null;
    }
}
