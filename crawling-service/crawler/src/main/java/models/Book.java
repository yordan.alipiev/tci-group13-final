package models;

import java.util.ArrayList;
import java.util.List;

/**
 * A class representing a book with a name, genre, year, authors,
 * publisher and isbn.
 */
public class Book {
    private String name;
    private String genre;
    private String year;
    private List<String> authors;
    private String publisher;
    private String isbn;
    private String format;

    /**
     * Initializes a new book.
     */
    public Book() {
        this.authors = new ArrayList<>();
    }

    /**
     * Gets the name of the book.
     *
     * @return Returns the name of the book.
     */
    public String getName() {
        return name;
    }

    /**
     * Gets the genre of the book.
     *
     * @return Returns the genre of the book.
     */
    public String getGenre() {
        return genre;
    }

    /**
     * Gets the publishing year of the book.
     *
     * @return Returns the publishing year of the book.
     */
    public String getYear() {
        return year;
    }

    /**
     * Gets the authors of the book.
     *
     * @return Returns a list of authors of the book.
     */
    public List<String> getAuthors() {
        return authors;
    }

    /**
     * Gets the publisher of the book.
     *
     * @return Returns the publisher of the book.
     */
    public String getPublisher() {
        return publisher;
    }

    /**
     * Gets the international standard book number of this book.
     *
     * @return Returns the international standard book number of this book.
     */
    public String getIsbn() {
        return isbn;
    }

    /**
     * Gets the format of the book.
     *
     * @return Returns the format of the book.
     */
    public String getFormat() {
        return format;
    }

    /**
     * Sets the format of the book.
     *
     * @param format The format to set.
     * @throws IllegalArgumentException in case the provided value
     *                                  was null or empty.
     */
    public void setFormat(String format) throws IllegalArgumentException {
        this.format = this.validateInput(format);
    }

    /**
     * Sets the name of the book.
     *
     * @param name The name to set.
     * @throws IllegalArgumentException in case the provided value
     *                                  was null or empty.
     */
    public void setName(String name) throws IllegalArgumentException {
        this.name = this.validateInput(name);
    }

    /**
     * Sets the genre of the book.
     *
     * @param genre The genre to set.
     * @throws IllegalArgumentException in case the provided value
     *                                  was null or empty.
     */
    public void setGenre(String genre) throws IllegalArgumentException {
        this.genre = this.validateInput(genre);
    }

    /**
     * Sets the year of publishing of the book.
     *
     * @param year The year to set.
     * @throws IllegalArgumentException in case the provided value was null
     * or empty or could not be parsed to an integer.
     */
    public void setYear(String year) throws IllegalArgumentException {
        String yearString = validateInput(year);
        try {
            Integer.parseInt(yearString);
            this.year = yearString;
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException();
        }
    }

    /**
     * Sets the authors of the book.
     *
     * @param authors The authors to set.
     * @throws IllegalArgumentException in case the provided value
     *                                  was null or empty.
     */
    public void setAuthors(List<String> authors)
            throws IllegalArgumentException {
        if (authors == null) {
            throw new IllegalArgumentException();
        }
        this.authors = authors;
    }

    /**
     * Sets the publisher of the book.
     *
     * @param publisher The publisher to set.
     * @throws IllegalArgumentException in case the provided value
     *                                  was null or empty.
     */
    public void setPublisher(String publisher) throws IllegalArgumentException {
        this.publisher = this.validateInput(publisher);
    }

    /**
     * Sets the isbn of the book.
     *
     * @param isbn The isbn to set.
     * @throws IllegalArgumentException in case the provided value
     *                                  was null or empty.
     */
    public void setIsbn(String isbn) throws IllegalArgumentException {
        this.isbn = this.validateInput(isbn);
    }

    /**
     * @return Trimmed representation of the provided string.
     * @throws IllegalArgumentException in case the provided value
     *                                  was null or empty.
     */
    private String validateInput(String input) throws IllegalArgumentException {
        if (input == null || input.trim().isEmpty()) {
            throw new IllegalArgumentException();
        }
        return input.trim();
    }
}
