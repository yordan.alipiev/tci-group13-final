package parsers;

import exceptions.ParsingException;
import helpers.MusicHelper;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import models.Music;
import org.jsoup.nodes.Element;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import org.junit.runner.RunWith;

import static custom.matchers.MusicAssert.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Tests the functionality of the {@link MusicParser} class.
 */
@RunWith(JUnitParamsRunner.class)
public class MusicParserTest {
    private MusicParser sut;
    private MusicHelper musicHelper;
    private static String NAME = "Complete Symphonies";

    /**
     * Creates a div element which has class media-details and a header 1 child with the value of Name variable.
     *
     * @return Div element.
     */
    private Element createMediaDetailsElementAndAppendName() {
        Element musicElement = new Element("div");
        musicElement.addClass("media-details");
        musicElement.appendElement("h1").text(NAME);
        return musicElement;
    }

    private static final String[] getMatchingValues() {
        MusicHelper helper = new MusicHelper();
        Music music = helper.getParsedMusic(NAME);
        return new String[]{
                music.getFormat(),
                music.getGenre(),
                music.getArtist(),
                music.getName(),
                music.getYear(),
        };
    }

    /**
     * Executes before each test.
     */
    @Before
    public void setUp() {
        sut = new MusicParser();
        musicHelper = new MusicHelper();
    }

    private static String watcherLog;

    @Rule
    public final TestRule watcher = new TestWatcher() {

        @Override
        protected void succeeded(Description description) {
            watcherLog += description.getDisplayName() + " " + "succeeded!\n";
        }

        @Override
        protected void failed(Throwable e, Description description) {
            watcherLog += description.getDisplayName() + "  failed because: " + e.getClass().getSimpleName() + "\n";
        }

        @Override
        protected void finished(Description description) {
            super.finished(description);
            System.out.println(watcherLog);
            watcherLog = "";
        }
    };

    /**
     * Tests that a {@link models.Music} can be parsed by providing standard element structure of a music.
     */
    @Test
    public void parseMusicSuccessfully() throws ParsingException {
        // Arrange
        Element musicElement = musicHelper.createMusicTableElement(NAME);
        Music expected = musicHelper.getParsedMusic(NAME);

        // Act
        Music actual = sut.parse(musicElement);

        // Assert
        assertThat(actual).hasArtist(expected.getArtist());
        assertThat(actual).hasFormat(expected.getFormat());
        assertThat(actual).hasGenre(expected.getGenre());
        assertThat(actual).hasName(expected.getName());
        assertThat(actual).hasYear(expected.getYear());
    }

    /**
     * Tests that parsing a {@link models.Music} with missing tags in the element throws
     * a {@link ParsingException}.
     */
    @Test(expected = ParsingException.class)
    public void parseMusicWithMissingTagsThrowsParsingException() throws ParsingException {
        // Arrange
        Element musicElement = createMediaDetailsElementAndAppendName();

        // Act
        sut.parse(musicElement);
    }

    /**
     * Tests that parsing a {@link models.Music} with extra tags in the element throws a
     * {@link ParsingException}.
     */
    @Test(expected = ParsingException.class)
    public void parseMusicWithExtraTagsThrowsParsingException() throws ParsingException {
        // Arrange
        Element musicElement = musicHelper.createMusicTableElement(NAME);
        musicElement.getElementsByTag("tbody").first().appendElement("tr").appendElement("th").text("Extra element");

        // Act
        sut.parse(musicElement);
    }

    /**
     * Tests that parsing a {@link models.Music} with an unknown property in the element throws a
     * {@link ParsingException}.
     */
    @Test(expected = ParsingException.class)
    public void parseMusicWithUnknownPropertyThrowsParsingException() throws ParsingException {
        // Arrange
        Element musicElement = createMediaDetailsElementAndAppendName();
        Element tbody = musicElement.appendElement("table").appendElement("tbody");
        Element row = tbody.appendElement("tr");
        row.appendElement("th").text("Book");
        row.appendElement("td").text("Music");

        // Act
        sut.parse(musicElement);
    }

    /**
     * Tests that parsing music with a wrong CSS class name of the wrapping div tag,
     * throws a {@link ParsingException}.
     *
     * @throws ParsingException
     */
    @Test(expected = ParsingException.class)
    public void parseMusicWithWrongDivClassNameThrowsParsingException() throws ParsingException {
        // Arrange
        Element musicElement = new Element("div");
        musicElement.addClass("wrongClass");

        // Act
        this.sut.parse(musicElement);
    }

    /**
     * Tests that parsing {@link Music} can be done by matching property.
     */
    @Test
    @Parameters(method = "getMatchingValues")
    public void parseMusicByMatchingProperty(String parameter) throws ParsingException {
        // Arrange
        Element musicElement = musicHelper.createMusicTableElement(NAME);
        Music expected = musicHelper.getParsedMusic(NAME);

        // Act
        Music actual = sut.parseByMatchingProperty(musicElement, parameter);

        // Assert
        assertNotNull("Property was not found in the object.", actual);
        assertThat(actual).hasArtist(expected.getArtist());
        assertThat(actual).hasFormat(expected.getFormat());
        assertThat(actual).hasGenre(expected.getGenre());
        assertThat(actual).hasName(expected.getName());
        assertThat(actual).hasYear(expected.getYear());
    }

    /**
     * Tests if null is returned after trying to parse {@link Music} with not matching property.
     */
    @Test
    public void failToParseMusicByMatchingProperty() throws ParsingException {
        // Arrange
        Element musicElement = musicHelper.createMusicTableElement(NAME);

        // Act
        Music actual = sut.parseByMatchingProperty(musicElement, "Some random name");

        // Assert
        assertEquals("Parsed music name does not match.", null, actual);
    }
}
