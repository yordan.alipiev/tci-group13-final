package services;

import exceptions.ParsingException;
import models.Music;
import models.MutableInteger;
import parsers.MusicParser;
import org.jsoup.nodes.Element;

import java.util.ArrayList;
import java.util.List;

/**
 * Provides functionality to retrieve Music from
 * {@link Element}.
 */
public class MusicService {

    /**
     * A Music parser object responsible for the parsing from
     * {@link Element} to a Music
     */
    private MusicParser musicParser;

    /**
     * Initializes Music service using a music parser object.
     *
     * @param musicParser Music parser which is going to be used to parse
     * {@link Element} to Music
     */
    public MusicService(MusicParser musicParser) {
        this.musicParser = musicParser;
    }

    /**
     * Initializes Music service.
     */
    public MusicService() {
        this.musicParser = new MusicParser();
    }

    /**
     * Retrieves all Music objects inside the provided elements.
     *
     * @param elements List of elements to be parsed.
     * @param pages The number of pages.
     * @return List of music objects.
     * @throws ParsingException Thrown by the parser.
     */
    public List<Music> getAllMusic(List<Element> elements, MutableInteger pages)
            throws ParsingException {
        List<Music> music = new ArrayList<>();

        for (Element element : elements) {
            pages.increment();
            music.add(getMusic(element));
        }
        return music;
    }

    /**
     * Retrieves a Music object which is inside the provided element
     *
     * @param element Provided element to search Music object.
     * @return Music object.
     * @throws ParsingException Thrown by the parser.
     */
    public Music getMusic(Element element) throws ParsingException {
        return musicParser.parse(element);
    }

    /**
     * Searches for a music with a matching property in the list of
     * provided elements.
     *
     * @param elements The elements which should be searched.
     * @param property The property to look for.
     * @param pages    Reference integer indicating the number of pages
     *                explored.
     * @return Returns a {@link Music} with a matching property or null,
     * if none is found.
     * @throws ParsingException Thrown by the {@link MusicParser}.
     */
    public Music searchMusic(
            List<Element> elements,
            String property,
            MutableInteger pages)
            throws ParsingException {
        for (Element e : elements) {
            pages.increment();
            Music music = this.musicParser.parseByMatchingProperty(e, property);
            if (music != null) {
                return music;
            }
        }

        return null;
    }
}
