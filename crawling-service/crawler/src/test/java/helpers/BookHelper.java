package helpers;

import models.Book;
import org.jsoup.nodes.Element;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class BookHelper {
    private static final String GENRE = "Tech";
    private static final String FORMAT = "Hardcover";
    private static final String YEAR = "1999";
    private static final String AUTHORS = "Martin Fowler, Kent Beck, John Brant, William Opdyke, Don Roberts";
    private static final String PUBLISHER = "Addison-Wesley Professional";
    private static final String ISBN = "978-0201485677";
    private static List<String> CONV_AUTHORS;

    public Element getBookElement(String name) {
        Element bookElement = new Element("div");
        bookElement.addClass("media-details");
        bookElement.appendElement("h1").text(name);

        Element tbody = bookElement.appendElement("table").appendElement("tbody");
        Element row = tbody.appendElement("tr");
        row.appendElement("th").text("Category");
        row.appendElement("td").text("Books");

        row = tbody.appendElement("tr");
        row.appendElement("th").text("Genre");
        row.appendElement("td").text(GENRE);

        row = tbody.appendElement("tr");
        row.appendElement("th").text("Format");
        row.appendElement("td").text(FORMAT);

        row = tbody.appendElement("tr");
        row.appendElement("th").text("Year");
        row.appendElement("td").text(YEAR);

        row = tbody.appendElement("tr");
        row.appendElement("th").text("Authors");
        row.appendElement("td").text(AUTHORS);

        row = tbody.appendElement("tr");
        row.appendElement("th").text("Publisher");
        row.appendElement("td").text(PUBLISHER);

        row = tbody.appendElement("tr");
        row.appendElement("th").text("ISBN");
        row.appendElement("td").text(ISBN);

        return bookElement;
    }

    public Book getParsedBook(String name) {
        Book parsed = new Book();

        CONV_AUTHORS = new ArrayList<>(Arrays.asList(AUTHORS.split(", ")));
        parsed.setFormat(FORMAT);
        parsed.setAuthors(CONV_AUTHORS);
        parsed.setIsbn(ISBN);
        parsed.setPublisher(PUBLISHER);
        parsed.setYear(YEAR);
        parsed.setGenre(GENRE);
        parsed.setName(name);

        return parsed;
    }
}
