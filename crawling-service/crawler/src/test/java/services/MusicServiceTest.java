package services;

import exceptions.ParsingException;
import helpers.MusicHelper;
import models.Music;
import models.MutableInteger;
import org.jsoup.nodes.Element;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.junit.rules.Timeout;
import parsers.MusicParser;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;

/**
 * Tests the functionality of {@link MusicService} class.
 */
public class MusicServiceTest {

    private MusicService sut;
    private MusicParser musicMockParser = mock(MusicParser.class);
    private MusicHelper musicHelper = new MusicHelper();

    @Rule
    public final TestRule globalTimeout = Timeout.seconds(5);

    /**
     * Executed every time before test.
     */
    @Before
    public void setUp() {
        sut = new MusicService(musicMockParser);
    }

    /**
     * Tests a happy scenario of retrieving all musics from a collection of {@link Element}
     * objects.
     */
    @Test
    public void getAllMusicSuccessfully() throws ParsingException {
        // Arrange
        List<Element> musicElements = new ArrayList<>();
        Element musicElement = this.musicHelper.createMusicTableElement("Elvis");
        musicElements.add(musicElement);
        musicElements.add(musicElement);
        musicElements.add(musicElement);

        Music expectedMusic = musicHelper.getParsedMusic("Elvis");

        List<Music> expectedMusics = new ArrayList<>();
        expectedMusics.add(expectedMusic);
        expectedMusics.add(expectedMusic);
        expectedMusics.add(expectedMusic);

        when(musicMockParser.parse(musicElement)).thenReturn(expectedMusic);

        // Act
        List<Music> actual = this.sut.getAllMusic(musicElements, new MutableInteger());

        // Assert
        assertThat("The actual size of retrieved music does not match the expected.", actual.size(), is(expectedMusics.size()));
        assertThat("The retrieved music do not match the expected.", actual, is(expectedMusics));
    }

    /**
     * Tests a happy scenario of retrieving a music from a {@link Element} object.
     */
    @Test
    public void getMusicSuccessfully() throws ParsingException {
        // Arrange
        Element musicElement = this.musicHelper.createMusicTableElement("Elvis");
        Music expectedMusic = musicHelper.getParsedMusic("Elvis");
        when(this.musicMockParser.parse(musicElement)).thenReturn(expectedMusic);

        // Act
        Music actual = sut.getMusic(musicElement);

        // Assert
        assertThat(actual, is(expectedMusic));
        verify(this.musicMockParser).parse(musicElement);
    }

    /**
     * Tests the functionality of the {@link MusicService} to search for a {@link Music} in a list of {@link Element} by providing a property.
     */
    @Test
    public void searchMusicSuccessfully() throws ParsingException {
        // Arrange
        List<Element> elements = new ArrayList<>();
        Element musicElement = this.musicHelper.createMusicTableElement("Elvis");
        Element randomElement = this.musicHelper.createMusicTableElement("Bon Jovi");
        Music expected = this.musicHelper.getParsedMusic("Elvis");
        elements.add(randomElement);
        elements.add(musicElement);
        when(this.musicMockParser.parseByMatchingProperty(musicElement, "Elvis")).thenReturn(expected);

        // Act
        Music actual = this.sut.searchMusic(elements, "Elvis", new MutableInteger());

        // Assert
        Assert.assertEquals("The expected music was not the same as the actual.", expected, actual);
        verify(this.musicMockParser).parseByMatchingProperty(musicElement, "Elvis");
    }

    /**
     * Tests the functionality of the {@link MusicService} to search for a {@link Music} in a list of {@link Element} by providing a property which does not exist.
     */
    @Test
    public void searchMusicWithNotExistingProperty() throws ParsingException {
        // Arrange
        List<Element> elements = new ArrayList<>();
        Element musicElement = this.musicHelper.createMusicTableElement("Elvis");
        elements.add(musicElement);
        sut = new MusicService();

        // Act
        Music actual = this.sut.searchMusic(elements, "Bon Jovi", new MutableInteger());

        // Assert
        Assert.assertEquals("The expected music was not the same as the actual.", null, actual);
    }
}
