package helpers;

import models.Movie;
import org.jsoup.nodes.Element;

import java.util.Arrays;

public class MovieHelper {
    private String GENRE = "Drama";
    private String FORMAT = "Blu-ray";
    private String YEAR = "2001";
    private String DIRECTOR = "Peter Jackson";
    private String[] WRITERS = new String[]{"J.R.R. Tolkien", "Fran Walsh", "Philippa Boyens", "Peter Jackson"};
    private String[] STARS = new String[]{"Ron Livingston", "Jennifer Aniston", "David Herman", "Ajay Naidu", "Diedrich Bader", "Stephen Root"};


    public Element createMovieTableForElement(String name) {
        Element movieElement = new Element("div");
        movieElement.addClass("media-details");
        movieElement.appendElement("h1").text(name);

        Element tbody = movieElement.appendElement("table").appendElement("tbody");
        Element row = tbody.appendElement("tr");
        row.appendElement("th").text("Category");
        row.appendElement("td").text("Movie");

        row = tbody.appendElement("tr");
        row.appendElement("th").text("Genre");
        row.appendElement("td").text(GENRE);

        row = tbody.appendElement("tr");
        row.appendElement("th").text("Format");
        row.appendElement("td").text(FORMAT);

        row = tbody.appendElement("tr");
        row.appendElement("th").text("Year");
        row.appendElement("td").text(YEAR);

        row = tbody.appendElement("tr");
        row.appendElement("th").text("Director");
        row.appendElement("td").text(DIRECTOR);

        row = tbody.appendElement("tr");
        row.appendElement("th").text("Writers");
        row.appendElement("td").text(String.join(", ", WRITERS));

        row = tbody.appendElement("tr");
        row.appendElement("th").text("Stars");
        row.appendElement("td").text(String.join(", ", STARS));

        return movieElement;
    }

    public Movie getParsedMovie(String name) {
        Movie parsed = new Movie();

        parsed.setFormat(FORMAT);
        parsed.getStars().addAll(Arrays.asList(STARS));
        parsed.setDirector(DIRECTOR);
        parsed.getWriters().addAll(Arrays.asList(WRITERS));
        parsed.setYear(YEAR);
        parsed.setGenre(GENRE);
        parsed.setName(name);

        return parsed;
    }


}
