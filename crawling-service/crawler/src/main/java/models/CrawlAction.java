package models;

import java.time.LocalTime;

/**
 * Represents a single crawling action performed over a given url.
 */
public class CrawlAction {
    private static int crawlingIdentifier = 0;

    private int id;
    private String strategy;
    private int pagesExplored;
    private int timeElapsed;
    private int searchDepth;
    private LocalTime timeStamp;

    /**
     * Constructs an object that keeps information about a single crawling
     * performed.
     *
     * @param strategy      The strategy used for the crawling action.
     * @param pagesExplored The pages explored during the crawling action.
     * @param timeElapsed   The time it took for the crawling action to
     *                     complete.
     * @param searchDepth   The search depth of the crawling action.
     */
    public CrawlAction(
            String strategy,
            int pagesExplored,
            int timeElapsed,
            int searchDepth) {
        this.id = crawlingIdentifier;
        crawlingIdentifier++;
        this.strategy = strategy;
        this.pagesExplored = pagesExplored;
        this.timeElapsed = timeElapsed;
        this.searchDepth = searchDepth;
        this.timeStamp = LocalTime.now();
    }

    /**
     * Gets the unique identifier of the crawling action.
     *
     * @return The unique identifier of the crawling action.
     */
    public int getId() {
        return id;
    }

    /**
     * Gets the strategy of the crawling action.
     *
     * @return The strategy of the crawling action.
     */
    public String getStrategy() {
        return strategy;
    }

    /**
     * Gets the pages explored by the crawling action.
     *
     * @return The pages explored by the crawling action.
     */
    public int getPagesExplored() {
        return pagesExplored;
    }

    /**
     * Gets the search depth of of the crawling action.
     *
     * @return The search depth of the crawling action.
     */
    public int getSearchDepth() {
        return searchDepth;
    }

    /**
     * Gets the time the crawling action was created.
     *
     * @return The time the crawling action was created.
     */
    public LocalTime getTimeStamp() {
        return timeStamp;
    }

    /**
     * Gets the duration of the crawling action in milliseconds.
     *
     * @return The time it took the crawling action to complete in milliseconds.
     */
    public int getTimeElapsed() {
        return timeElapsed;
    }
}
