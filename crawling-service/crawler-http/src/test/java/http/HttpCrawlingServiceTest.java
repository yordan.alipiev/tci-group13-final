package http;

import exceptions.DoesNotExistException;
import exceptions.InvalidBaseAddressException;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import models.*;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.junit.rules.Timeout;
import org.junit.runner.RunWith;
import services.CrawlingService;

import javax.ws.rs.core.Response;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * A test class for {@link HttpCrawlingService}.
 */
@RunWith(JUnitParamsRunner.class)
public class HttpCrawlingServiceTest {

    private HttpCrawlingService sut;
    private CrawlingService mockCrawlingService;
    private Book dummyBook = new Book();
    private Movie dummyMovie = new Movie();
    private Music dummyMusic = new Music();
    private Class sutClass;
    private Class[] getObjectCallArgsTypes =
            {String.class, String.class, String.class};
    private static final String baseAddress = "https://i339805.hera.fhict.nl";

    @Rule
    public final TestRule globalTimeout = Timeout.seconds(10);

    /**
     * Facilitates the passing of string parameters to the tests.
     */
    static class TestParameter {
        public String property;
        public String type;

        /**
         * Instantiates a test parameter.
         *
         * @param property The property to set.
         * @param type     The type to set.
         */
        public TestParameter(String property, String type) {
            this.property = property;
            this.type = type;
        }
    }

    /**
     * Gets a list of parameters which will cause a NoContent response
     * when used to get an object from the service.
     *
     * @return An array of test parameters.
     */
    private static TestParameter[] getNoContentValues() {
        return new TestParameter[]{
                new TestParameter("dummy", "book"),
                new TestParameter("dummy", "movie"),
                new TestParameter("dummy", "music"),
        };
    }

    /**
     * Gets a list of parameters which exist and can be retrieved
     * from the website.
     *
     * @return An array of test parameters.
     */
    private static TestParameter[] getSuccessValues() {
        return new TestParameter[]{
                new TestParameter("Programmers", "book"),
                new TestParameter("Lord", "movie"),
                new TestParameter("Elvis", "music"),
        };
    }

    /**
     * Tests if an invalid property would cause bad request.
     */
    @Before
    public void setUp() {
        sut = new HttpCrawlingService();
        sutClass = sut.getClass();
        mockCrawlingService = mock(CrawlingService.class);
        dummyBook.setName("dummy");
        dummyMovie.setName("dummy");
        dummyMusic.setName("dummy");
    }

    /**
     * Tests if an invalid property would cause bad request.
     */
    @Test
    public void getObjectShouldInvalidPropertyShouldReturnBadRequest()
            throws NoSuchMethodException,
            InvocationTargetException,
            IllegalAccessException,
            NoSuchFieldException,
            InvalidBaseAddressException,
            DoesNotExistException,
            IOException {
        // Arrange
        String[] arguments = {baseAddress, "Programmers", "test"};

        when(mockCrawlingService.getItem(any(String.class), any(String.class), any(String.class)))
                .thenThrow(DoesNotExistException.class);

        Field field = this.sutClass.getDeclaredField("service");
        field.setAccessible(true);
        field.set(sut, mockCrawlingService);

        Method getObjectCall = this.sutClass.getDeclaredMethod("getObject", getObjectCallArgsTypes);
        getObjectCall.setAccessible(true);

        Response response = (Response) getObjectCall.invoke(sut, arguments);

        assertThat(response.getStatus(), is(400));
    }

    /**
     * Tests if a book, movie and music can be successfully retrieved by given property.
     */
    @Test
    @Parameters(method = "getSuccessValues")
    public void getObjectShouldSuccessfullyReturnTheRequiredObject(TestParameter testParameter)
            throws InvalidBaseAddressException,
            DoesNotExistException,
            IOException,
            NoSuchFieldException,
            IllegalAccessException,
            NoSuchMethodException,
            InvocationTargetException {
        // Arrange
        String[] args = {baseAddress, testParameter.property, testParameter.type};

        SiteData expected = new SiteData();
        expected.setBooks(Arrays.asList(dummyBook));
        expected.setMovies(Arrays.asList(dummyMovie));
        expected.setMusic(Arrays.asList(dummyMusic));

        when(mockCrawlingService.getItem(any(String.class), any(String.class), any(String.class)))
                .thenReturn(expected);

        Field field = sutClass.getDeclaredField("service");
        field.setAccessible(true);
        field.set(sut, mockCrawlingService);

        // Check the hash map is empty upon initialization
        Field crawlActionHashmap = sutClass.getDeclaredField("crawlActions");
        crawlActionHashmap.setAccessible(true);
        HashMap<CrawlAction, SiteData> actual = (HashMap<CrawlAction, SiteData>) crawlActionHashmap.get(sut);
        assertThat(actual.size(), is(0));

        Method getObjectCall = sutClass.getDeclaredMethod("getObject", getObjectCallArgsTypes);
        getObjectCall.setAccessible(true);

        // Act
        Response response = (Response) getObjectCall.invoke(sut, args);

        actual = (HashMap<CrawlAction, SiteData>) crawlActionHashmap.get(sut);

        // Assert
        assertThat(actual.size(), is(1));
        assertThat(response.getStatus(), is(200));
        assertThat(response.getEntity(), is(expected));
    }

    /**
     * Tests if NoContentResponse will be returned in case no object of a type with a matching
     * property is found.
     */
    @Test
    @Parameters(method = "getNoContentValues")
    public void getObjectShouldReturnNoContentResponseInCaseNoObjectIsFound(TestParameter testParameter)
            throws InvalidBaseAddressException,
            DoesNotExistException,
            IOException,
            NoSuchFieldException,
            IllegalAccessException,
            NoSuchMethodException,
            InvocationTargetException {
        // Arrange
        String[] arguments = {baseAddress, testParameter.property, testParameter.type};

        SiteData expected = new SiteData();
        expected.setBooks(Arrays.asList(new Book[]{null}));
        expected.setMovies(Arrays.asList(new Movie[]{null}));
        expected.setMusic(Arrays.asList(new Music[]{null}));
        when(mockCrawlingService.getItem(any(String.class), any(String.class), any(String.class)))
                .thenReturn(expected);

        Field field = sutClass.getDeclaredField("service");
        field.setAccessible(true);
        field.set(sut, mockCrawlingService);

        // Check the hash-map
        Field crawlActionsHashmap = sutClass.getDeclaredField("crawlActions");
        crawlActionsHashmap.setAccessible(true);
        HashMap<CrawlAction, SiteData> actual = (HashMap<CrawlAction, SiteData>) crawlActionsHashmap.get(sut);

        assertThat(actual.size(), is(0));

        Method getObjectCall = sutClass.getDeclaredMethod("getObject", this.getObjectCallArgsTypes);
        getObjectCall.setAccessible(true);

        // Act.
        Response response = (Response) getObjectCall.invoke(sut, arguments);

        actual = (HashMap<CrawlAction, SiteData>) crawlActionsHashmap.get(sut);

        // Assert
        assertThat(actual.size(), is(1));
        assertThat(response.getStatus(), is(204));
    }

    /**
     * Tests if a bad request will be returned in case the base url did not contain
     * the desired website.
     */
    @Test
    public void getSiteDataShouldReturnBadRequestInCaseOfInvalidBaseUrl()
            throws InvalidBaseAddressException,
            NoSuchFieldException,
            IllegalAccessException,
            NoSuchMethodException,
            InvocationTargetException {
        // Arrange
        when(mockCrawlingService.getAllSiteData(any(String.class)))
                .thenThrow(InvalidBaseAddressException.class);
        Field field = this.sutClass.getDeclaredField("service");
        field.setAccessible(true);
        field.set(sut, mockCrawlingService);

        Method getObjectCall = this.sutClass.getDeclaredMethod("getSiteData", String.class);
        getObjectCall.setAccessible(true);

        // Act
        Response response = (Response) getObjectCall.invoke(sut, "https://gitlab.com/");

        // Assert
        assertThat(response.getStatus(), is(400));
    }

    /**
     * Tests if the getSiteData would return all movies, books and music from the base url.
     */
    @Test
    public void getSiteDataShouldReturnAllSiteData()
            throws InvalidBaseAddressException,
            NoSuchFieldException,
            IllegalAccessException,
            NoSuchMethodException,
            InvocationTargetException {
        // Arrange
        SiteData expected = new SiteData();
        expected.setBooks(Arrays.asList(dummyBook));
        expected.setMovies(Arrays.asList(dummyMovie));
        expected.setMusic(Arrays.asList(dummyMusic));

        when(mockCrawlingService.getAllSiteData(any(String.class))).thenReturn(expected);
        Field field = this.sutClass.getDeclaredField("service");
        field.setAccessible(true);
        field.set(sut, mockCrawlingService);

        Method getObjectCall = this.sutClass.getDeclaredMethod("getSiteData", String.class);
        getObjectCall.setAccessible(true);

        // Act
        Response response = (Response) getObjectCall.invoke(sut, baseAddress);

        // Assert
        assertThat(response.getStatus(), is(200));
        assertThat(response.getEntity(), is(expected));
    }

    /**
     * Tests if the getAllCrawlActions would return all crawl actions.
     */
    @Test
    public void getAllCrawlActionsShouldReturnAllCrawlActions()
            throws NoSuchFieldException,
            IllegalAccessException,
            NoSuchMethodException,
            InvocationTargetException,
            InvalidBaseAddressException {
        // Arrange
        SiteData siteData = new SiteData();
        siteData.setBooks(Arrays.asList(dummyBook));
        siteData.setMovies(Arrays.asList(dummyMovie));
        siteData.setMusic(Arrays.asList(dummyMusic));

        when(mockCrawlingService.getAllSiteData(any(String.class))).thenReturn(siteData);

        Field field = this.sutClass.getDeclaredField("service");
        field.setAccessible(true);
        field.set(sut, mockCrawlingService);

        Method getSiteData = this.sutClass.getDeclaredMethod("getSiteData", String.class);
        getSiteData.setAccessible(true);

        // Invoke two crawl actions
        getSiteData.invoke(sut, baseAddress);
        getSiteData.invoke(sut, baseAddress);

        Method getAllCrawlActions = this.sutClass.getDeclaredMethod("getAllCrawlActions", null);
        getAllCrawlActions.setAccessible(true);

        // Act
        Response response = (Response) getAllCrawlActions.invoke(sut, null);
        List<CrawlAction> actualCrawlActions = (List<CrawlAction>) response.getEntity();

        // Assert
        assertThat(response.getStatus(), is(200));
        assertThat(actualCrawlActions.size(), is(2));
    }

    /**
     * Tests if the getLatestCrawlAction returns the last performed crawl action.
     */
    @Test
    public void getLatestCrawlActionSuccessfully()
            throws InvalidBaseAddressException,
            NoSuchFieldException,
            IllegalAccessException,
            NoSuchMethodException,
            InvocationTargetException {
        // Arrange
        SiteData siteData = new SiteData();
        siteData.setBooks(Arrays.asList(dummyBook));
        siteData.setMovies(Arrays.asList(dummyMovie));
        siteData.setMusic(Arrays.asList(dummyMusic));
        siteData.setDepth(2);
        siteData.setPagesExplored(2);

        when(mockCrawlingService.getAllSiteData(any(String.class))).thenReturn(siteData);

        Field field = this.sutClass.getDeclaredField("service");
        field.setAccessible(true);
        field.set(sut, mockCrawlingService);

        Method getSiteData = this.sutClass.getDeclaredMethod("getSiteData", String.class);
        getSiteData.setAccessible(true);
        Method getLatestCrawlAction = this.sutClass.getDeclaredMethod("getLatestCrawlAction", null);
        getLatestCrawlAction.setAccessible(true);

        // Invoke a crawl action
        getSiteData.invoke(sut, baseAddress);

        // Act
        Response response = (Response) getLatestCrawlAction.invoke(sut, null);
        CrawlAction actual = (CrawlAction) response.getEntity();

        // Assert
        assertThat(response.getStatus(), is(200));
        assertThat(actual.getPagesExplored(), is(siteData.getPagesExplored()));
        assertThat(actual.getSearchDepth(), is(siteData.getDepth()));
        assertThat(actual.getStrategy(), is("DFS"));
    }

    /**
     * Tests that getLatestCrawlAction returns a NoContent response when there are no crawl actions.
     */
    @Test
    public void getLatestCrawlActionNoContent()
            throws NoSuchMethodException,
            InvocationTargetException,
            IllegalAccessException {
        Method getLatestCrawlAction = this.sutClass.getDeclaredMethod("getLatestCrawlAction", null);
        getLatestCrawlAction.setAccessible(true);

        // Act
        Response response = (Response) getLatestCrawlAction.invoke(sut, null);

        // Assert
        assertThat(response.getStatus(), is(204));
    }

    /**
     * Tests if no content response is returned in case no crawl action is found with the given id.
     */
    @Test
    public void getCrawlActionShouldReturnNoContentResponseIfNoneIsFound()
            throws IllegalAccessException,
            NoSuchMethodException,
            InvocationTargetException {
        // Arrange
        Method getCrawlAction = this.sutClass.getDeclaredMethod("getCrawlAction", int.class);
        getCrawlAction.setAccessible(true);

        // Act
        Response response = (Response) getCrawlAction.invoke(sut, 1);

        // Assert
        assertThat(response.getStatus(), is(400));
    }

    /**
     * Tests if the getCrawlAction returns the correct {@link CrawlAction}.
     */
    @Test
    public void getCrawlActionSuccessfully()
            throws NoSuchMethodException,
            InvocationTargetException,
            IllegalAccessException,
            InvalidBaseAddressException,
            NoSuchFieldException {
        // Arrange
        SiteData siteData = new SiteData();
        siteData.setBooks(Arrays.asList(dummyBook));
        siteData.setMovies(Arrays.asList(dummyMovie));
        siteData.setMusic(Arrays.asList(dummyMusic));

        when(mockCrawlingService.getAllSiteData(any(String.class))).thenReturn(siteData);

        Field field = this.sutClass.getDeclaredField("service");
        field.setAccessible(true);
        field.set(sut, mockCrawlingService);

        Method getSiteData = this.sutClass.getDeclaredMethod("getSiteData", String.class);
        getSiteData.setAccessible(true);

        Method getCrawlAction = this.sutClass.getDeclaredMethod("getCrawlAction", int.class);
        getCrawlAction.setAccessible(true);

        // Invoke a crawl action
        getSiteData.invoke(sut, baseAddress);

        // Get all the crawl actions
        Method getAllCrawlActions = this.sutClass.getDeclaredMethod("getAllCrawlActions", null);
        getAllCrawlActions.setAccessible(true);
        Response crawlActionResponse = (Response) getAllCrawlActions.invoke(sut, null);
        CrawlAction expectedCrawlAction = ((List<CrawlAction>) crawlActionResponse.getEntity()).get(0);

        // Act
        Response response = (Response) getCrawlAction.invoke(sut, expectedCrawlAction.getId());
        CrawlAction actual = (CrawlAction) response.getEntity();

        // Assert
        assertThat(response.getStatus(), is(200));
        assertThat(actual, is(expectedCrawlAction));
    }
}
