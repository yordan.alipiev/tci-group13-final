package models;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.TestCase.assertEquals;

/**
 * Tests the functionality of {@link SiteData} class.
 */
public class SiteDataTest {

    SiteData sut;
    private static String DUMMYVALUE = "Testing value";

    /**
     * Executed every time before each test.
     * Initializes an object of type {@link SiteData}.
     */
    @Before
    public void setUp() {
        sut = new SiteData();
    }

    /**
     * Tests if the list of {@link Movie} is set correctly.
     */
    @Test
    public void collectionOfMoviesIsSetCorrectly() {
        // Arrange
        List<Movie> collectionOfMovies = new ArrayList<>();
        Movie movie = new Movie();
        movie.setName(DUMMYVALUE);
        movie.setDirector(DUMMYVALUE);
        movie.setGenre(DUMMYVALUE);
        movie.setFormat(DUMMYVALUE);
        collectionOfMovies.add(movie);

        // Act
        sut.setMovies(collectionOfMovies);

        // Assert
        assertEquals("The setting of the collection does not contain correct Movie object", sut.getMovies().get(0), movie);
    }


    /**
     * Tests if the list of {@link Music} is set correctly.
     */
    @Test
    public void collectionOfMusicIsSetCorrectly() {
        // Arrange
        List<Music> collectionOfMusic = new ArrayList<>();
        Music music = new Music();
        music.setName(DUMMYVALUE);
        music.setArtist(DUMMYVALUE);
        music.setFormat(DUMMYVALUE);
        music.setGenre(DUMMYVALUE);
        collectionOfMusic.add(music);

        // Act
        sut.setMusic(collectionOfMusic);

        // Assert
        assertEquals("The setting of the collection does not contain correct Music object", sut.getMusic().get(0), music);
    }

    /**
     * Tests if the list of {@link Book} is set correctly.
     */
    @Test
    public void collectionOfBooksIsSetCorrectly() {
        // Arrange
        List<Book> collectionOfBooks = new ArrayList<>();
        Book book = new Book();
        book.setName(DUMMYVALUE);
        book.setGenre(DUMMYVALUE);
        collectionOfBooks.add(book);

        // Act
        sut.setBooks(collectionOfBooks);

        // Assert
        assertEquals("The setting of the collection does not contain correct Book object", sut.getBooks().get(0), book);
    }

    /**
     * Tests if you can set empty list instead of list full of {@link Movie}.
     */
    @Test
    public void emptyCollectionCanBeSetMovies() {
        // Arrange
        List<Movie> collectionOfMovies = new ArrayList<>();

        // Act
        sut.setMovies(collectionOfMovies);

        // Assert
        assertEquals("Setting of empty collection was not possible", sut.getMovies(), collectionOfMovies);
    }

    /**
     * Tests if you can set empty list instead of list full of {@link Music}.
     */
    @Test
    public void emptyCollectionCanBeSetMusic() {
        // Arrange
        List<Music> collectionOfMusic = new ArrayList<>();

        // Act
        sut.setMusic(collectionOfMusic);

        // Assert
        assertEquals("Setting of empty collection was not possible", sut.getMusic(), collectionOfMusic);
    }

    /**
     * Tests if you can set empty list instead of list full of {@link Book}.
     */
    @Test
    public void emptyCollectionCanBeSetBooks() {
        // Arrange
        List<Book> collectionOfBooks = new ArrayList<>();

        // Act
        sut.setBooks(collectionOfBooks);

        // Assert
        assertEquals("Setting of empty collection was not possible", sut.getBooks(), collectionOfBooks);
    }

    /**
     * Tests if you can set and get the depth of the site data.
     */
    @Test
    public void depthCanBeSetForTheSiteData() {
        // Arrange
        int expectedDepth = 3;

        // Act
        sut.setDepth(3);

        // Assert
        assertEquals("Something went wrong with the setting up of the depth.", sut.getDepth(), expectedDepth);
    }

    /**
     * Tests if you can set and get the number of pages explored for the site data.
     */
    @Test
    public void pagesExploredCanBeSetForTheSiteData() {
        // Arrange
        int expectedPagesExplored = 3;

        // Act
        sut.setPagesExplored(3);

        // Assert
        assertEquals("Something went wrong with the setting up of the number of pages explored.",
                sut.getPagesExplored(),
                expectedPagesExplored);
    }
}