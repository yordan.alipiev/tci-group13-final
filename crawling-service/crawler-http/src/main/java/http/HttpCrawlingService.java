package http;

import models.*;
import services.CrawlingService;

import javax.inject.Singleton;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.*;

/**
 * Web service providing crawling functionality that works with the
 * example website provided for the Fontys TCI course 2018.
 */
@Path("crawling")
@Singleton
public class HttpCrawlingService {

    private HashMap<CrawlAction, SiteData> crawlActions = new HashMap<>();
    private CrawlingService service = new CrawlingService();

    /**
     * Crawls through the provided base address and searches
     * for a book, movie or music, depending on the provided type,
     * that has any property matching the provided property string.
     * Returns the first discovered object as JSON.
     *
     * @param baseAddress The base address to be crawled.
     * @param property    The value of a property of the object.
     * @param type        the case insensitive type of the object.
     *                    Either book, movie or music.
     * @return In case of invalid type, returns 400 Bad request,
     * else 200 with the found object.
     * In case the property was fine, but no object was found 204
     * No content response.
     */
    @GET
    @Path("getObject")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getObject(
            @QueryParam("baseAddress") String baseAddress,
            @QueryParam("property") String property,
            @QueryParam("type") String type) {

        type = type.toLowerCase().trim();
        Date start = Calendar.getInstance().getTime();
        if (baseAddress != null && !baseAddress.endsWith("/")) {
            baseAddress += "/";
        }

        SiteData data = new SiteData();
        try {
            data = service.getItem(baseAddress, type, property);
        } catch (Exception e) {
            Date end = Calendar.getInstance().getTime();
            logCrawlAction(
                    "DFS",
                    (int) (end.getTime() - start.getTime()),
                    1,
                    1,
                    null);
            return Response.status(Response.Status.BAD_REQUEST)
                    .entity(e.getMessage()).build();
        }
        Date end = Calendar.getInstance().getTime();
        logCrawlAction(
                "DFS",
                (int) (end.getTime() - start.getTime()),
                data.getDepth(),
                data.getPagesExplored(),
                data);

        Book bookResult = null;
        Music musicResult = null;
        Movie movieResult = null;
        if (type.equals("book")) {
            bookResult = data.getBooks().get(0);
        } else if (type.equals("music")) {
            musicResult = data.getMusic().get(0);
        } else if (type.equals("movie")) {
            movieResult = data.getMovies().get(0);
        }

        if (bookResult == null && musicResult == null && movieResult == null) {
            return Response.status(Response.Status.NO_CONTENT).build();
        }
        return Response.status(Response.Status.OK).entity(data).build();
    }

    /**
     * Crawls through the provided base address and gets all of the found
     * information about movies, music and books. Returns the information in
     * a JSON format.
     *
     * @param baseAddress The base address to be crawled.
     * @return 200 with all the scraped information about movies, books
     * and music as a {@link SiteData} object. 400 Bad request in case
     * of invalid base url.
     */
    @GET
    @Path("getSiteData")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getSiteData(@QueryParam("baseAddress") String baseAddress) {
        Date start = Calendar.getInstance().getTime();
        if (baseAddress != null && !baseAddress.endsWith("/")) {
            baseAddress += "/";
        }

        try {
            SiteData siteData = this.service.getAllSiteData(baseAddress);

            // Log crawl action in case of success.
            Date end = Calendar.getInstance().getTime();
            logCrawlAction(
                    "DFS",
                    (int) (end.getTime() - start.getTime()),
                    siteData.getDepth(),
                    siteData.getPagesExplored(),
                    siteData);

            return Response.status(Response.Status.OK).entity(siteData).build();
        } catch (Exception e) {
            // Log crawl action in case of failure.
            Date end = Calendar.getInstance().getTime();
            logCrawlAction(
                    "DFS",
                    (int) (end.getTime() - start.getTime()),
                    1,
                    1,
                    null);

            return Response.status(Response.Status.BAD_REQUEST)
                    .entity(e.getMessage()).build();
        }
    }

    /**
     * Gets information about the latest crawl action performed by the service.
     *
     * @return The latest crawl action performed by the service.
     */
    @GET
    @Path("getLatestCrawlAction")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getLatestCrawlAction() {
        try {
            CrawlAction lastCrawlAction = this.crawlActions.keySet()
                    .stream()
                    .max(Comparator.comparing(CrawlAction::getTimeStamp))
                    .get();

            return Response.status(Response.Status.OK)
                    .entity(lastCrawlAction).build();
        } catch (Exception e) {
            return Response.status(Response.Status.NO_CONTENT).build();
        }
    }

    /**
     * Gets the information about a single crawl action that was previously
     * performed.
     *
     * @param id The unique identifier of the crawl action.
     * @return 200 with information about the crawl action as
     * {@link CrawlAction} object.
     * 204 is returned, in case no crawl action matching the id.
     */
    @GET
    @Path("getCrawlAction")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCrawlAction(@QueryParam("id") int id) {
        CrawlAction crawlAction = null;
        for (CrawlAction ca : this.crawlActions.keySet()) {
            if (ca.getId() == id) {
                crawlAction = ca;
            }
        }

        if (crawlAction == null) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        return Response.status(Response.Status.OK)
                .entity(crawlAction).build();
    }

    /**
     * Gets all crawl actions performed by the crawling service since it was
     * initialized.
     *
     * @return All crawl actions performed by the service in the form of a
     * collection of
     * {@link CrawlAction} objects.
     */
    @GET
    @Path("getAllCrawlActions")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllCrawlActions() {
        List<CrawlAction> allCrawlActions = new ArrayList<>(
                this.crawlActions.keySet());
        return Response.status(Response.Status.OK).entity(allCrawlActions)
                .build();
    }

    private void logCrawlAction(
            String strategy,
            int elapsedTime,
            int searchDepth,
            int pagesExplored,
            SiteData data) {
        CrawlAction crawlAction = new CrawlAction(
                strategy, pagesExplored, elapsedTime, searchDepth);
        crawlActions.put(crawlAction, data);
    }
}
