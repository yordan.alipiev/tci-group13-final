package models;

/**
 * A class representing a music with a name, format, year and artist.
 */
public class Music {

    private String name;
    private String genre;
    private String format;
    private String year;
    private String artist;

    /**
     * Initializes new Music class
     */
    public Music() {

    }

    /**
     * Returns the name of the song.
     *
     * @return name of the song
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the name of the song.
     *
     * @param name The name of the song to be set.
     * @throws IllegalArgumentException in case the provided value was
     * null or empty.
     */
    public void setName(String name) throws IllegalArgumentException {
        this.name = validateInput(name);
    }

    /**
     * Gets the genre of the song.
     *
     * @return genre of the song.
     */
    public String getGenre() {
        return genre;
    }

    /**
     * Sets the genre of the song./
     *
     * @param genre The genre of the song to be set.
     */
    public void setGenre(String genre) {
        this.genre = validateInput(genre);
    }

    /**
     * Returns the format of the song.
     *
     * @return format of the song.
     */
    public String getFormat() {
        return format;
    }

    /**
     * Sets the format of the song.
     *
     * @param format of the song.
     * @throws IllegalArgumentException in case the provided value was
     * null or empty.
     */
    public void setFormat(String format) throws IllegalArgumentException {
        this.format = validateInput(format);
    }

    /**
     * Returns the year when the song was made.
     *
     * @return year of the song.
     */
    public String getYear() {
        return year;
    }

    /**
     * Sets the year when the song was made.
     *
     * @param year of the song.
     * @throws IllegalArgumentException in case the provided value was
     * null or empty.
     */
    public void setYear(String year) throws IllegalArgumentException {
        this.year = validateInput(year);
    }

    /**
     * Returns the artist who made the song.
     *
     * @return artist of the song.
     */
    public String getArtist() {
        return artist;
    }

    /**
     * Sets the artist of the song.
     *
     * @param artist artist of the song.
     * @throws IllegalArgumentException in case the provided value was
     * null or empty.
     */
    public void setArtist(String artist) throws IllegalArgumentException {
        this.artist = validateInput(artist);
    }

    /**
     * Validates the provided input for null empty or white spaces.
     *
     * @param input string that needs to be validated
     * @return Validated trimmed input.
     * @throws IllegalArgumentException Only if the input is null, empty
     * or contains only white spaces.
     */
    private String validateInput(String input) throws IllegalArgumentException {
        if (input == null || input.trim().isEmpty()) {
            throw new IllegalArgumentException();
        }
        return input.trim();
    }
}
