package exceptions;

/**
 * Exception that might occur while attempting to crawl an address
 * that does not host the proper website.
 */
public class InvalidBaseAddressException extends Exception {

    /**
     * Instantiates a new {@link InvalidBaseAddressException}.
     *
     * @param message The message of the exception.
     */
    public InvalidBaseAddressException(String message) {
        super(message);
    }
}
