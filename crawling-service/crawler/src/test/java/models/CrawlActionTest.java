package models;

import org.junit.Before;
import org.junit.Test;

import java.time.LocalTime;

import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

/**
 * Tests functionality of {@link CrawlAction} class.
 */
public class CrawlActionTest {
    private static final String STRATEGY = "BFS";
    private static final int PAGES = 3;
    private static final int ELAPSED_TIME = 300;
    private static final int SEARCH_DEPTH = 18;

    private CrawlAction sut;

    @Before
    public void setUp() {
        // Arrange
        sut = new CrawlAction(STRATEGY, PAGES, ELAPSED_TIME, SEARCH_DEPTH);
    }

    /**
     * Tests if the id of crawlAction is being auto incremented correctly.
     */
    @Test
    public void theIdOfCrawlActionsIsAutoIncremented() {
        // Act
        int firstId = new CrawlAction(STRATEGY, PAGES, ELAPSED_TIME, SEARCH_DEPTH).getId();
        int secondId = new CrawlAction(STRATEGY, PAGES, ELAPSED_TIME, SEARCH_DEPTH).getId();

        // Assert
        assertTrue(firstId < secondId);
    }

    /**
     * Tests when a new crawl action is initialized with an incremented by one id.
     */
    @Test
    public void objectIsInitializedWithCorrectId() {
        // Act
        int firstId = new CrawlAction(STRATEGY, PAGES, ELAPSED_TIME, SEARCH_DEPTH).getId();
        int secondId = new CrawlAction(STRATEGY, PAGES, ELAPSED_TIME, SEARCH_DEPTH).getId();

        // Assert
        assertThat(secondId, is(firstId + 1));
    }

    /**
     * Tests if the constructor can accept null values for nullable data types.
     */
    @Test
    public void crawlActionCanAcceptNullObjectsInConstructor() {
        // Act
        new CrawlAction(null, PAGES, ELAPSED_TIME, SEARCH_DEPTH);
        new CrawlAction(STRATEGY, PAGES, 0, SEARCH_DEPTH);
    }

    /**
     * Tests if correct strategy will be returned.
     */
    @Test
    public void getStrategyShouldGetTheOneSetInTheConstructor() {
        // Act
        String actual = sut.getStrategy();

        // Assert
        assertThat("The property did not have a correct value.", actual, is(STRATEGY));
    }


    /**
     * Tests if correct number of pages explored will be returned.
     */
    @Test
    public void getPagesExploredShouldGetTheNumberSetInTheConstructor() {
        // Act
        int actual = sut.getPagesExplored();

        // Assert
        assertThat("The property did not have a correct value.", actual, is(PAGES));
    }

    /**
     * Tests if correct search depth will be returned.
     */
    @Test
    public void getSearchDepthShouldGetTheNumberSetInTheConstructor() {
        // Act
        int actual = sut.getSearchDepth();

        // Assert
        assertThat("The property did not have a correct value.", actual, is(SEARCH_DEPTH));
    }

    /**
     * Tests if correct time elapsed will be returned.
     */
    @Test
    public void getTimeElapsedShouldGetTheDurationInMillisecondsSetInTheConstructor() {
        // Act
        int actual = sut.getTimeElapsed();

        // Assert
        assertThat("The property did not have a correct value.", actual, is(ELAPSED_TIME));
    }

    /**
     * Tests if correct creation time will be returned.
     */
    @Test
    public void getTimeStampShouldReturnTheTimestampSetInTheConstructor() {
        // Act
        LocalTime actual = sut.getTimeStamp();

        // Assert
        assertThat("The property did not have a correct value.", actual, is(greaterThan(LocalTime.now().minusMinutes(1))));
    }
}
