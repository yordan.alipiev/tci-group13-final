package models;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

import static org.junit.Assert.assertThat;

import java.util.ArrayList;

import static org.hamcrest.Matchers.*;
import static custom.matchers.MovieAssert.assertThat;

import org.junit.*;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;

/**
 * Tests the functionality of the {@link Movie} class.
 */
@RunWith(JUnitParamsRunner.class)
public class MovieTests {
    Movie sut;

    @Rule
    public final ExpectedException thrown = ExpectedException.none();

    @Before
    public void setUp() {
        // Arrange
        sut = new Movie();
    }

    /**
     * Facilitates the passing of string parameters to the tests.
     */
    static class TestParameter {
        public String caseValue;

        /**
         * Creates a test parameter object containing a string.
         */
        public TestParameter(String caseValue) {
            this.caseValue = caseValue;
        }
    }

    /**
     * Gets a list of the test parameters that would cause
     * {@link IllegalArgumentException} when used as a set
     * value for any property of a book.
     */
    private static final TestParameter[] getExceptionValues() {
        return new TestParameter[]{
                new TestParameter(null),
                new TestParameter(""),
                new TestParameter("                ")
        };
    }

    /**
     * Gets a list of valid values with different length that can be set
     * for a property of a movie without causing an exception.
     */
    private static final TestParameter[] getSettableValues() {
        return new TestParameter[]{
                new TestParameter(" a"),
                new TestParameter("  String value"),
                new TestParameter("A Long string value that can be any property of a Movie. "),
        };
    }

    /**
     * Tests that setting the name of a movie to null or empty throws an
     * {@link IllegalArgumentException}.
     */
    @Test
    @Parameters(method = "getExceptionValues")
    public void setMovieNameNullOrEmptyThrowsIllegalArgumentException(TestParameter parameter) {
        thrown.expect(IllegalArgumentException.class);
        // Act
        sut.setName(parameter.caseValue);
    }

    /**
     * Tests that setting the genre of a movie to null or empty throws an
     * {@link IllegalArgumentException}.
     */
    @Test
    @Parameters(method = "getExceptionValues")
    public void setMovieGenreNullOrEmptyThrowsIllegalArgumentException(TestParameter parameter) {
        thrown.expect(IllegalArgumentException.class);
        // Act
        sut.setGenre(parameter.caseValue);
    }

    /**
     * Tests that setting the format of a movie to null or empty throws an
     * {@link IllegalArgumentException}.
     */
    @Test
    @Parameters(method = "getExceptionValues")
    public void setMovieFormatNullOrEmptyThrowsIllegalArgumentException(TestParameter parameter) {
        thrown.expect(IllegalArgumentException.class);
        // Act
        sut.setFormat(parameter.caseValue);
    }

    /**
     * Tests that setting the year of a movie to null or empty throws an
     * {@link IllegalArgumentException}.
     */
    @Test
    @Parameters(method = "getExceptionValues")
    public void setMovieYearNullOrEmptyThrowsIllegalArgumentException(TestParameter parameter) {
        thrown.expect(IllegalArgumentException.class);
        // Act
        sut.setYear(parameter.caseValue);
    }

    /**
     * Tests that setting the director of a movie to null or empty throws an
     * {@link IllegalArgumentException}.
     */
    @Test
    @Parameters(method = "getExceptionValues")
    public void setMovieDirectorNullOrEmptyThrowsIllegalArgumentException(TestParameter parameter) {
        thrown.expect(IllegalArgumentException.class);
        // Act
        sut.setDirector(parameter.caseValue);
    }

    /**
     * Tests that the list of writers is initialized and is empty, on object creation.
     */
    @Test
    public void movieConstructorShouldInitializeAnEmptyListOfWriters() {
        // Act
        ArrayList<String> actual = sut.getWriters();

        // Assert. 
        assertThat("The writers were not initialized.", actual, is(notNullValue()));
        assertThat("The writers were not initialized empty.", actual, is(empty()));
    }

    /**
     * Tests that the list of stars is initialized and is empty, on object creation.
     */
    @Test
    public void movieConstructorShouldInitializeAnEmptyListOfStars() {
        // Act
        ArrayList<String> actual = sut.getStars();

        // Assert. 
        assertThat("The stars were not initialized.", actual, is(notNullValue()));
        assertThat("The stars were not initialized empty.", actual, is(empty()));
    }

    /**
     * Tests that the list of writers can be publicly manipulated.
     */
    @Test
    public void addingMovieWriterShouldBePossibleThroughGetWriters() {
        // Arrange
        String dummyWriter = "Dummy Writer";

        // Act
        ArrayList<String> writers = sut.getWriters();
        writers.add(dummyWriter);
        ArrayList<String> actual = sut.getWriters();

        // Assert        
        assertThat("The writer was not added to the list.", actual, is(not(empty())));
        assertThat("The item did not exist in the list.", actual, is(hasItem(dummyWriter)));
        assertThat(sut).hasWriters(writers);
    }

    /**
     * Tests that the list of stars can be publicly manipulated.
     */
    @Test
    public void addingMovieStarsShouldBePossibleThroughGetStars() {
        // Arrange
        String dummyStar = "Dummy Star";

        // Act
        ArrayList<String> stars = sut.getStars();
        stars.add(dummyStar);
        ArrayList<String> actual = sut.getStars();

        // Assert        
        assertThat("The star was not added to the list.", actual, is(not(empty())));
        assertThat("The item did not exist in the list.", actual, is(hasItem(dummyStar)));
        assertThat(sut).hasStars(stars);
    }

    /**
     * Tests that setting the name of a movie actually changes the
     * property of the {@link Movie}.
     */
    @Test
    @Parameters(method = "getSettableValues")
    public void setMovieNameShouldResultInAPropertyChange(TestParameter parameter) {
        // Act
        sut.setName(parameter.caseValue);

        // Assert
        assertThat(sut).hasName(parameter.caseValue.trim());
    }

    /**
     * Tests that setting the genre of a movie actually changes the
     * property of the {@link Movie}.
     */
    @Test
    @Parameters(method = "getSettableValues")
    public void setMovieGenreShouldResultInAPropertyChange(TestParameter parameter) {
        // Act
        sut.setGenre(parameter.caseValue);

        // Assert
        assertThat(sut).hasGenre(parameter.caseValue.trim());
    }

    /**
     * Tests that setting the format of a movie actually changes the
     * property of the {@link Movie}.
     */
    @Test
    @Parameters(method = "getSettableValues")
    public void setMovieFormatShouldResultInAPropertyChange(TestParameter parameter) {
        // Act
        sut.setFormat(parameter.caseValue);

        // Assert
        assertThat(sut).hasFormat(parameter.caseValue.trim());
    }

    /**
     * Tests that setting the year of a movie with a string that is not a valid integer
     * would throw {@link IllegalArgumentException}
     */
    @Test
    @Parameters(method = "getSettableValues")
    public void setMovieYearShouldThrowIllegalArgumentExceptionWithInvalidYear(TestParameter parameter) {
        thrown.expect(IllegalArgumentException.class);
        // Act
        sut.setYear(parameter.caseValue);
    }

    /**
     * Tests that setting the year of a movie actually changes the
     * property of the {@link Movie}.
     */
    @Test
    public void setMovieYearShouldResultInAPropertyChange() {
        // Arrange
        String dummyYear = "1996";

        // Act
        sut.setYear(dummyYear);

        // Assert        
        assertThat(sut).hasYear(dummyYear);
    }

    /**
     * Tests that setting the director of a movie actually changes the
     * property of the {@link Movie}.
     */
    @Test
    @Parameters(method = "getSettableValues")
    public void setMovieDirectorShouldResultInAPropertyChange(TestParameter parameter) {
        // Act
        sut.setDirector(parameter.caseValue);

        // Assert
        assertThat(sut).hasDirector(parameter.caseValue.trim());
    }
}