package services;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.junit.rules.Timeout;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;

/**
 * Tests the functionality of the {@link ScrapingService} class.
 */
public class ScrapingServiceTest {
    private ScrapingService sut;
    private MusicService musicServiceMock = mock(MusicService.class);
    private MovieService movieServiceMock = mock(MovieService.class);
    private BookService bookServiceMock = mock(BookService.class);

    @Rule
    public final TestRule globalTimeout = Timeout.seconds(5);

    @Before
    public void setUp() {
        this.sut = new ScrapingService(bookServiceMock, movieServiceMock, musicServiceMock);
    }

    /**
     * Tests that {@link MusicService} is assigned correctly upon initialization of
     * {@link ScrapingService}.
     */
    @Test
    public void musicServiceIsAssignedCorrectly() {
        assertThat(
                "Music service was not assigned correctly.",
                this.sut.getMusicService(),
                is(musicServiceMock));
    }

    /**
     * Tests that {@link MovieService} is assigned correctly upon initialization of
     * {@link ScrapingService}.
     */
    @Test
    public void movieServiceIsAssignedCorrectly() {
        assertThat(
                "Movie service was not assigned correctly.",
                this.sut.getMovieService(),
                is(movieServiceMock));
    }

    /**
     * Tests that {@link BookService} is assigned correctly upon initialization of
     * {@link ScrapingService}.
     */
    @Test
    public void bookServiceIsAssignedCorrectly() {
        assertThat(
                "Book service was not assigned correctly.",
                this.sut.getBookService(),
                is(bookServiceMock));
    }
}