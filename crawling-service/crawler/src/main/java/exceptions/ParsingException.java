package exceptions;

/**
 * Exception that might occur while parsing an
 * {@link org.jsoup.nodes.Element} to a given type.
 */
public class ParsingException extends Exception {

    /**
     * Instantiates a new {@link ParsingException}.
     */
    public ParsingException() {
        super("Error occurred while trying to parse object.");
    }

    /**
     * Instantiates a new {@link ParsingException}.
     *
     * @param message The message of the exception.
     */
    public ParsingException(String message) {
        super(message);
    }
}
