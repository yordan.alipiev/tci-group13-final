package services;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Provides functionality to retrieve DOM elements by exposing functionality
 * from JSoup library.
 */
public class JSoupService {
    /**
     * Retrieves an {@link Element} with the provided CSS class name from a
     * given URL.
     *
     * @param url The URL to retrieve the element from.
     * @param className The CSS class name of the element.
     * @return Returns an {@link Element} with the provided CSS class name.
     * @throws IOException Thrown by JSoup library.
     */
    public Element getElementFromUrlWithClass(String url, String className)
            throws IOException {
        Document doc = Jsoup.connect(url).get();
        return doc.getElementsByClass(className).first();
    }

    /**
     * Retrieves a collection of {@link Element} objects from the given URL.
     * It is assumed all elements are in DOM objects with CSS class
     * "media-details".
     *
     * @param url The URL to retrieve elements from.
     * @return Returns a collection of {@link Element} objects in the provided
     * address.
     * @throws IOException Thrown by JSoup library.
     */
    public List<Element> getElementsFromUrl(String url) throws IOException {
        List<String> urls = this.getUrlsFromUrl(url);
        List<Element> elements = new ArrayList<>();
        String[] splitUrl = url.split("/");
        String baseAddress = String.join("/", Arrays
                .copyOfRange(splitUrl, 0, splitUrl.length - 1));
        for (String u : urls) {
            elements.add(this.getElementFromUrlWithClass(
                    baseAddress + "/" + u, "media-details"));
        }

        return elements;
    }

    /**
     * Retrieves all sub-URLs from the given URL. It is assumed the searched for
     * URL links are placed in a <ul> tag with CSS class "items" in the DOM.
     *
     * @param url The URL to search in.
     * @return Returns a collection of URLs in the given URL.
     * @throws IOException Thrown by JSoup library.
     */
    public List<String> getUrlsFromUrl(String url) throws IOException {
        List<String> urls = new ArrayList<>();
        Document doc = Jsoup.connect(url).get();
        Element ul = doc.getElementsByClass("items").first();
        List<Element> allAElements = ul.select("a");

        for (Element a : allAElements) {
            urls.add(a.attr("href"));
        }

        return urls;
    }
}
