package parsers;

import exceptions.ParsingException;
import org.jsoup.nodes.Element;

/**
 * Generic interface exposing a method for parsing an {@link Element}.
 *
 * @param <T> The type of the class.
 */
public interface IGenericParser<T> {
    /**
     * Parses the provided {@code element}.
     *
     * @param element The element to parse.
     * @return Returns a parsed object.
     * @throws ParsingException in case a property of the type was null
     *                          after the complete parse.
     */
    T parse(Element element) throws ParsingException;

    /**
     * Parses an {@link Element} into a single  object of type T and
     * looks for a property matching the provided one.
     *
     * @param element  The element to parse.
     * @param property The property to look for.
     * @return Returns an object of type T if a matching property is found,
     * else null.
     * @throws ParsingException in case a property is null.
     */
    T parseByMatchingProperty(Element element, String property)
            throws ParsingException;
}
