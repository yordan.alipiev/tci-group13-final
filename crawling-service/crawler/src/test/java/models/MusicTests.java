package models;


import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;

import static custom.matchers.MusicAssert.assertThat;
import static org.junit.Assert.*;

/**
 * Tests the functionality of the {@link Music} class
 */
@RunWith(JUnitParamsRunner.class)
public class MusicTests {
    Music sut;

    @Rule
    public final ExpectedException thrown = ExpectedException.none();

    /**
     * Runs every time before test.
     */
    @Before
    public void setUp() {
        sut = new Music();
    }

    /**
     * Facilitates the passing of string parameters to the tests.
     */
    static class TestParameter {
        public String caseValue;

        /**
         * Creates a test parameter object containing a string.
         */
        public TestParameter(String caseValue) {
            this.caseValue = caseValue;
        }
    }

    /**
     * Gets a list of the test parameters that would cause
     * {@link IllegalArgumentException} when used as a set
     * value for any property of a {@link Music}.
     */
    private static final MusicTests.TestParameter[] getExceptionValues() {
        return new MusicTests.TestParameter[]{
                new MusicTests.TestParameter(null),
                new MusicTests.TestParameter(""),
                new MusicTests.TestParameter("                ")
        };
    }

    /**
     * Gets a list of valid values with different length that can be set
     * for a property of a {@link Music} without causing an exception.
     */
    private static final MusicTests.TestParameter[] getSettableValues() {
        return new MusicTests.TestParameter[]{
                new MusicTests.TestParameter("a"),
                new MusicTests.TestParameter("  String value"),
                new MusicTests.TestParameter("A Long string value that can be any property of a Music. "),
        };
    }

    /**
     * Tests if the constructor creates the object without setting by default name.
     */
    @Test
    public void constructorShouldInitializeWithEmptyName() {
        //Assert
        assertNull("Name is not empty", sut.getName());
    }

    /**
     * Tests if the constructor creates the object without setting by default format.
     */
    @Test
    public void constructorShouldInitializeWithEmptyFormat() {
        //Assert
        assertNull("Format is not empty", sut.getFormat());
    }

    /**
     * Tests if the constructor creates the object without setting by default year.
     */
    @Test
    public void constructorShouldInitializeWithEmptyYear() {
        assertNull("Year is not empty", sut.getYear());
    }

    /**
     * Tests if the constructor creates the object without setting by default artist.
     */
    @Test
    public void constructorShouldInitializeWithEmptyArtist() {
        assertNull("Artist is not empty", sut.getYear());
    }

    /**
     * Tests if the name property is being set correctly.
     */
    @Test
    @Parameters(method = "getSettableValues")
    public void setNameShouldResultInPropertyValueChange(TestParameter parameter) {
        // Act
        sut.setName(parameter.caseValue);

        // Assert
        assertThat(sut).hasName(parameter.caseValue.trim());
    }

    /**
     * Tests if the format property is being set correctly.
     */
    @Test
    @Parameters(method = "getSettableValues")
    public void setFormatShouldResultInPropertyValueChange(TestParameter parameter) {
        // Act
        sut.setFormat(parameter.caseValue);

        // Assert
        assertThat(sut).hasFormat(parameter.caseValue.trim());
    }

    /**
     * Tests if the year property is being set correctly.
     */
    @Test
    @Parameters(method = "getSettableValues")
    public void setYearShouldResultInPropertyValueChange(TestParameter parameter) {
        // Act
        sut.setYear(parameter.caseValue);

        // Assert
        assertThat(sut).hasYear(parameter.caseValue.trim());
    }

    /**
     * Tests if the artist property is being set correctly.
     */
    @Test
    @Parameters(method = "getSettableValues")
    public void setArtistShouldResultInPropertyValueChange(TestParameter parameter) {
        // Act
        sut.setArtist(parameter.caseValue);

        // Assert
        assertThat(sut).hasArtist(parameter.caseValue.trim());
    }

    /**
     * Tests if the name property is throwing {@link IllegalArgumentException} when white space, giving null or empty.
     */
    @Test
    @Parameters(method = "getExceptionValues")
    public void setNameShouldThrowException(TestParameter parameter) {
        thrown.expect(IllegalArgumentException.class);
        // Act
        sut.setName(parameter.caseValue);
    }

    /**
     * Tests if the format property is throwing {@link IllegalArgumentException} when white space, giving null or empty.
     */
    @Test
    @Parameters(method = "getExceptionValues")
    public void setFormatShouldThrowException(TestParameter parameter) {
        thrown.expect(IllegalArgumentException.class);
        // Act
        sut.setFormat(parameter.caseValue);
    }

    /**
     * Tests if the year property is throwing {@link IllegalArgumentException} when giving white space, null or empty.
     */
    @Test
    @Parameters(method = "getExceptionValues")
    public void setYearShouldThrowException(TestParameter parameter) {
        thrown.expect(IllegalArgumentException.class);
        // Act
        sut.setYear(parameter.caseValue);
    }

    /**
     * Tests if the artist property is throwing {@link IllegalArgumentException} when giving white space, null or empty.
     */
    @Test
    @Parameters(method = "getExceptionValues")
    public void setArtistShouldThrowException(TestParameter parameter) {
        thrown.expect(IllegalArgumentException.class);
        //Act
        sut.setArtist(parameter.caseValue);
    }
}
