package models;

import java.util.List;

/**
 * Represents all the data present in a website.
 */
public class SiteData {
    private int depth;
    private int pagesExplored;
    private List<Movie> movies;
    private List<Music> music;
    private List<Book> books;

    /**
     * Gets all movies.
     *
     * @return Returns a collection of {@link Movie} objects.
     */
    public List<Movie> getMovies() {
        return movies;
    }

    /**
     * Sets the movies of the site data.
     *
     * @param movies The movies to set.
     */
    public void setMovies(List<Movie> movies) {
        this.movies = movies;
    }

    /**
     * Gets all music.
     *
     * @return Returns a collection of {@link Music} objects.
     */
    public List<Music> getMusic() {
        return music;
    }

    /**
     * Sets the music of the site data.
     *
     * @param music The music to set.
     */
    public void setMusic(List<Music> music) {
        this.music = music;
    }

    /**
     * Gets all books.
     *
     * @return Returns a collection of {@link Book} objects.
     */
    public List<Book> getBooks() {
        return books;
    }

    /**
     * Sets the books of the site data.
     *
     * @param books The books to set.
     */
    public void setBooks(List<Book> books) {
        this.books = books;
    }

    /**
     * Gets the pages explored for retrieving this site data.
     *
     * @return The pages explored for retrieving this site data.
     */
    public int getPagesExplored() {
        return pagesExplored;
    }

    /**
     * Sets the pages explored to retrieve this ste data.
     *
     * @param pagesExplored The pages explored to retrieve this ste data.
     */
    public void setPagesExplored(int pagesExplored) {
        this.pagesExplored = pagesExplored;
    }

    /**
     * Gets the search depth for retrieving this ste data.
     *
     * @return The search depth for retrieving this ste data.
     */
    public int getDepth() {
        return depth;
    }

    /**
     * Sets the search depth for retrieving this ste data.
     *
     * @param depth The search depth for retrieving this ste data.
     */
    public void setDepth(int depth) {
        this.depth = depth;
    }
}
