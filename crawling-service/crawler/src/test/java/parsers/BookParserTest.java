package parsers;

import exceptions.ParsingException;
import helpers.BookHelper;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import models.Book;
import org.jsoup.nodes.Element;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import org.junit.runner.RunWith;

import static custom.matchers.BookAssert.assertThat;
import static org.junit.Assert.assertEquals;

/**
 * Tests the functionality of the {@link BookParser} class.
 */
@RunWith(JUnitParamsRunner.class)
public class BookParserTest {
    private static final String NAME = "The name of the book";
    private BookParser bookParser;
    private BookHelper bookHelper;

    private static String watcherLog;

    /**
     * Creates a div element with the provided class and a header 1 child with the value of Name variable.
     *
     * @param className The class for the div element.
     * @return Div element.
     */
    private Element createMediaDetailsElementAndAppendName(String className) {
        Element bookElement = new Element("div");
        bookElement.addClass(className);
        bookElement.appendElement("h1").text(NAME);
        return bookElement;
    }

    private static final String[] getMatchingValues() {
        BookHelper helper = new BookHelper();
        Book b = helper.getParsedBook(NAME);
        return new String[]{
                b.getFormat(),
                b.getGenre(),
                b.getIsbn(),
                b.getName(),
                b.getPublisher(),
                b.getYear(),
                b.getAuthors().get(0)
        };
    }

    @Rule
    public final TestRule watcher = new TestWatcher() {

        @Override
        protected void succeeded(Description description) {
            watcherLog += description.getDisplayName() + " " + "succeeded!\n";
        }

        @Override
        protected void failed(Throwable e, Description description) {
            watcherLog += description.getDisplayName() + "  failed because: " + e.getClass().getSimpleName() + "\n";
        }

        @Override
        protected void finished(Description description) {
            super.finished(description);
            System.out.println(watcherLog);
            watcherLog = "";
        }
    };

    /**
     * Executes actions before each test.
     */
    @Before
    public void setUp() {
        bookParser = new BookParser();
        bookHelper = new BookHelper();
    }

    /**
     * Tests that a book is parsed successfully by providing the standard
     * element structure of a book.
     */
    @Test
    public void parseBookSuccessfully() throws ParsingException {
        // Arrange
        Element bookElement = bookHelper.getBookElement(NAME);
        Book expected = bookHelper.getParsedBook(NAME);

        // Act
        Book book = bookParser.parse(bookElement);

        // Assert
        assertThat(book).hasName(expected.getName());
        assertThat(book).hasAuthors(expected.getAuthors());
        assertThat(book).hasFormat(expected.getFormat());
        assertThat(book).hasGenre(expected.getGenre());
        assertThat(book).hasISBN(expected.getIsbn());
        assertThat(book).hasPublisher(expected.getPublisher());
        assertThat(book).hasYear(expected.getYear());
    }

    /**
     * Tests that parsing a book with a wrong CSS class name of the wrapping div tag,
     * throws a {@link ParsingException}.
     *
     * @throws ParsingException
     */
    @Test(expected = ParsingException.class)
    public void parseBookWithWrongDivClassNameThrowsParsingException() throws ParsingException {
        // Arrange
        Element bookElement = createMediaDetailsElementAndAppendName("wrongClass");

        // Act
        bookParser.parse(bookElement);
    }

    /**
     * Tests that parsing a book with missing tags in the element throws
     * a {@link ParsingException}.
     */
    @Test(expected = ParsingException.class)
    public void parseBookWithMissingTagsThrowsParsingException() throws ParsingException {
        // Arrange
        Element bookElement = createMediaDetailsElementAndAppendName("media-details");

        // Act
        bookParser.parse(bookElement);
    }

    /**
     * Tests that parsing a book with an unknown property in the element throws a
     * {@link ParsingException}.
     */
    @Test(expected = ParsingException.class)
    public void parseBookWithUnknownPropertyThrowsParsingException() throws ParsingException {
        // Arrange
        Element bookElement = createMediaDetailsElementAndAppendName("media-details");

        // Create table
        Element tbody = bookElement.appendElement("table").appendElement("tbody");
        Element row = tbody.appendElement("tr");
        row.appendElement("th").text("Unknown");
        row.appendElement("td").text("Unknown");

        // Act
        bookParser.parse(bookElement);
    }

    /**
     * Tests that parsing {@link Book} can be done by matching property.
     */
    @Test
    @Parameters(method = "getMatchingValues")
    public void parseBookByMatchingProperty(String parameter) throws ParsingException {
        // Arrange
        Element musicElement = bookHelper.getBookElement(NAME);
        Book expected = bookHelper.getParsedBook(NAME);

        // Act
        Book actual = bookParser.parseByMatchingProperty(musicElement, parameter);

        // Assert
        assertThat(actual).hasName(expected.getName());
        assertThat(actual).hasAuthors(expected.getAuthors());
        assertThat(actual).hasFormat(expected.getFormat());
        assertThat(actual).hasGenre(expected.getGenre());
        assertThat(actual).hasISBN(expected.getIsbn());
        assertThat(actual).hasPublisher(expected.getPublisher());
        assertThat(actual).hasYear(expected.getYear());
    }


    /**
     * Tests if null is returned after trying to parse {@link Book} with not matching property.
     */
    @Test
    public void failToParseBookByMatchingProperty() throws ParsingException {
        // Arrange
        Element musicElement = bookHelper.getBookElement(NAME);

        // Act
        Book actual = bookParser.parseByMatchingProperty(musicElement, "Some random name");

        // Assert
        assertEquals("Parsed movie name does not match.", null, actual);
    }
}