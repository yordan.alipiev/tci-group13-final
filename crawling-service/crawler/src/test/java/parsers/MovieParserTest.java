package parsers;

import exceptions.ParsingException;
import helpers.MovieHelper;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import models.Movie;
import org.jsoup.nodes.Element;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import org.junit.runner.RunWith;

import static custom.matchers.MovieAssert.assertThat;
import static org.junit.Assert.assertEquals;

/**
 * Tests the functionality of the {@link MovieParser} class.
 */
@RunWith(JUnitParamsRunner.class)
public class MovieParserTest {
    private MovieParser sut;
    private MovieHelper movieHelper;
    private Element movieElement;

    private static String NAME = "The Lord of the Rings: The Fellowship of the Ring";

    /**
     * Creates a div element which has class media-details and a header 1 child with the value of Name variable.
     *
     * @return Div element.
     */
    private Element createMediaDetailsElementAndAppendName() {
        Element movieElement = new Element("div");
        movieElement.addClass("media-details");
        movieElement.appendElement("h1").text(NAME);
        return movieElement;
    }

    private static final String[] getMatchingValues() {
        MovieHelper helper = new MovieHelper();
        Movie movie = helper.getParsedMovie(NAME);
        return new String[]{
                movie.getFormat(),
                movie.getGenre(),
                movie.getWriters().get(0),
                movie.getName(),
                movie.getDirector(),
                movie.getYear(),
                movie.getStars().get(0),
        };
    }

    /**
     * Executes before each test.
     */
    @Before
    public void setUp() {
        sut = new MovieParser();
        movieElement = createMediaDetailsElementAndAppendName();
        movieHelper = new MovieHelper();
    }

    private static String watcherLog;

    @Rule
    public final TestRule watcher = new TestWatcher() {

        @Override
        protected void succeeded(Description description) {
            watcherLog += description.getDisplayName() + " " + "succeeded!\n";
        }

        @Override
        protected void failed(Throwable e, Description description) {
            watcherLog += description.getDisplayName() + "  failed because: " + e.getClass().getSimpleName() + "\n";
        }

        @Override
        protected void finished(Description description) {
            super.finished(description);
            System.out.println(watcherLog);
            watcherLog = "";
        }
    };

    /**
     * Tests that a movie is parsed successfully by providing the standard element
     * structure of a movie.
     *
     * @throws ParsingException
     */
    @Test
    public void parseMovieSuccessfully() throws ParsingException {
        // Arrange
        movieElement = movieHelper.createMovieTableForElement(NAME);
        Movie expected = movieHelper.getParsedMovie(NAME);

        // Act
        Movie movie = sut.parse(movieElement);

        // Assert
        assertThat(movie).hasName(expected.getName());
        assertThat(movie).hasGenre(expected.getGenre());
        assertThat(movie).hasFormat(expected.getFormat());
        assertThat(movie).hasYear(expected.getYear());
        assertThat(movie).hasDirector(expected.getDirector());
        assertThat(movie).hasWriters(expected.getWriters());
        assertThat(movie).hasStars(expected.getStars());
    }

    /**
     * Tests that parsing a movie with missing tags in the element throws a
     * {@link ParsingException}.
     *
     * @throws ParsingException
     */
    @Test(expected = ParsingException.class)
    public void parseMovieWithMissingTagsThrowsParsingException() throws ParsingException {
        // Act
        sut.parse(movieElement);
    }

    /**
     * Tests that parsing a movie with extra tags in the element throws a
     * {@link ParsingException}.
     *
     * @throws ParsingException
     */
    @Test(expected = ParsingException.class)
    public void parseMovieWithExtraTagsThrowsParsingException() throws ParsingException {
        // Arrange
        movieElement = movieHelper.createMovieTableForElement(NAME);
        movieElement.getElementsByTag("tbody").first().appendElement("tr").appendElement("th").text("Extra element");

        // Act
        sut.parse(movieElement);
    }

    /**
     * Tests that parsing a movie with an unknown property in the element throws a
     * {@link ParsingException}.
     *
     * @throws ParsingException
     */
    @Test(expected = ParsingException.class)
    public void parseMovieWithUnknownPropertyThrowsParsingException() throws ParsingException {
        // Arrange
        Element tbody = movieElement.appendElement("table").appendElement("tbody");
        Element row = tbody.appendElement("tr");
        row.appendElement("th").text("Book");
        row.appendElement("td").text("Movie");

        // Act
        sut.parse(movieElement);
    }

    /**
     * Tests that parsing a movie with a wrong CSS class name of the wrapping div tag,
     * throws a {@link ParsingException}.
     *
     * @throws ParsingException
     */
    @Test(expected = ParsingException.class)
    public void parseMovieWithWrongDivClassNameThrowsParsingException() throws ParsingException {
        // Arrange
        Element movieElement = new Element("div");
        movieElement.addClass("wrongClass");

        // Act
        this.sut.parse(movieElement);
    }

    /**
     * Tests that parsing {@link Movie} can be done by matching property.
     */
    @Test
    @Parameters(method = "getMatchingValues")
    public void parseMovieByMatchingProperty(String parameter) throws ParsingException {
        // Arrange
        Element musicElement = movieHelper.createMovieTableForElement(NAME);
        Movie expected = movieHelper.getParsedMovie(NAME);

        // Act
        Movie actual = sut.parseByMatchingProperty(musicElement, parameter);

        // Assert
        assertThat(actual).hasName(expected.getName());
        assertThat(actual).hasGenre(expected.getGenre());
        assertThat(actual).hasFormat(expected.getFormat());
        assertThat(actual).hasYear(expected.getYear());
        assertThat(actual).hasDirector(expected.getDirector());
        assertThat(actual).hasWriters(expected.getWriters());
        assertThat(actual).hasStars(expected.getStars());
    }

    /**
     * Tests if null is returned after trying to parse {@link Movie} with not matching property.
     */
    @Test
    public void failToParseMovieByMatchingProperty() throws ParsingException {
        // Arrange
        Element musicElement = movieHelper.createMovieTableForElement(NAME);

        // Act
        Movie actual = sut.parseByMatchingProperty(musicElement, "Some random name");

        // Assert
        assertEquals("Parsed movie name does not match.", null, actual);
    }
}