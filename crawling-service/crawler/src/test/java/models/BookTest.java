package models;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.List;

import static custom.matchers.BookAssert.assertThat;

/**
 * Tests the functionality of the {@link Book} class.
 */
@RunWith(JUnitParamsRunner.class)
public class BookTest {
    private Book book;

    @Rule
    public final ExpectedException thrown = ExpectedException.none();

    @Before
    public void setUp() {
        // Arrange
        book = new Book();
    }

    /**
     * Facilitates the passing of string parameters to the tests.
     */
    static class TestParameter {
        public String caseValue;

        /**
         * Creates a test parameter object containing a string.
         */
        public TestParameter(String caseValue) {
            this.caseValue = caseValue;
        }
    }

    /**
     * Gets a list of the test parameters that would cause
     * {@link IllegalArgumentException} when used as a set
     * value for any property of a book.
     */
    private static final TestParameter[] getExceptionValues() {
        return new TestParameter[]{
                new TestParameter(null),
                new TestParameter(""),
                new TestParameter("                ")
        };
    }

    /**
     * Gets a list of valid values with different length that can be set
     * for a property of a book without causing an exception.
     */
    private static final TestParameter[] getSettableValues() {
        return new TestParameter[]{
                new TestParameter("1"),
                new TestParameter("String value"),
                new TestParameter("A long string value that can be any property of a book."),
        };
    }

    /**
     * Gets a list of non-integer string values that would cause
     * {@link IllegalArgumentException} when used as a set value for book year.
     */
    private static final TestParameter[] getNonIntegerStrings() {
        return new TestParameter[]{
                new TestParameter("string"),
                new TestParameter("9.11"),
                new TestParameter("true"),
        };
    }

    /**
     * Tests that setting the name of a book to null or empty string throws an
     * {@link IllegalArgumentException}.
     */
    @Test
    @Parameters(method = "getExceptionValues")
    public void setBookNameNullOrEmptyThrowsIllegalArgumentException(TestParameter parameter) {
        thrown.expect(IllegalArgumentException.class);
        // Act
        book.setName(parameter.caseValue);
    }

    /**
     * Tests that setting the publisher of a book to null or empty string throws an
     * {@link IllegalArgumentException}.
     */
    @Test
    @Parameters(method = "getExceptionValues")
    public void setPublisherNullOrEmptyThrowsIllegalArgumentException(TestParameter parameter) {
        thrown.expect(IllegalArgumentException.class);
        // Act
        book.setPublisher(parameter.caseValue);
    }

    /**
     * Tests that setting the ISBN of a book to null or empty string throws an
     * {@link IllegalArgumentException}.
     */
    @Test
    @Parameters(method = "getExceptionValues")
    public void setISBNNullOrEmptyThrowsIllegalArgumentException(TestParameter parameter) {
        thrown.expect(IllegalArgumentException.class);
        // Act
        book.setIsbn(parameter.caseValue);
    }

    /**
     * Tests that setting the genre of a book to null or empty string throws an
     * {@link IllegalArgumentException}.
     */
    @Test
    @Parameters(method = "getExceptionValues")
    public void setGenreNullOrEmptyThrowsIllegalArgumentException(TestParameter parameter) {
        thrown.expect(IllegalArgumentException.class);
        // Act
        book.setGenre(parameter.caseValue);
    }

    /**
     * Tests that setting the year of a book to null or empty string throws an
     * {@link IllegalArgumentException}.
     */
    @Test
    @Parameters(method = "getExceptionValues")
    public void setYearNullOrEmptyThrowsIllegalArgumentException(TestParameter parameter) {
        thrown.expect(IllegalArgumentException.class);
        // Act
        book.setYear(parameter.caseValue);
    }

    /**
     * Tests that setting the year of a book to a non-integer throws {@link IllegalArgumentException}.
     */
    @Test
    @Parameters(method = "getNonIntegerStrings")
    public void setYearNonIntegerThrowsIllegalArgumentException(TestParameter parameter) {
        thrown.expect(IllegalArgumentException.class);
        // Act
        book.setYear(parameter.caseValue);
    }

    /**
     * Tests that setting the authors of a book to null throws an {@link IllegalArgumentException}.
     */
    @Test
    public void setAuthorsNullThrowsIllegalArgumentException() {
        thrown.expect(IllegalArgumentException.class);
        // Act
        book.setAuthors(null);
    }

    /**
     * Tests that setting the format of a book to null throws an {@link IllegalArgumentException}.
     */
    @Test
    public void setFormatNullThrowsIllegalArgumentException() {
        thrown.expect(IllegalArgumentException.class);
        // Act
        book.setFormat(null);
    }

    /**
     * Tests that setting the name of a book actually changes the
     * property of the {@link Book}.
     */
    @Test
    @Parameters(method = "getSettableValues")
    public void setBookNameShouldResultInAPropertyChange(TestParameter parameter) {
        // Act
        book.setName(parameter.caseValue);

        // Assert
        assertThat(book).hasName(parameter.caseValue);
    }

    /**
     * Tests that setting the publisher of a book actually changes the
     * property of the {@link Book}.
     */
    @Test
    @Parameters(method = "getSettableValues")
    public void setBookPublisherShouldResultInAPropertyChange(TestParameter parameter) {
        // Act
        book.setPublisher(parameter.caseValue);

        // Assert
        assertThat(book).hasPublisher(parameter.caseValue);
    }

    /**
     * Tests that setting the ISBN of a book actually changes the
     * property of the {@link Book}.
     */
    @Test
    @Parameters(method = "getSettableValues")
    public void setBookISBNShouldResultInAPropertyChange(TestParameter parameter) {
        // Act
        book.setIsbn(parameter.caseValue);

        // Assert
        assertThat(book).hasISBN(parameter.caseValue);
    }

    /**
     * Tests that setting the genre of a book actually changes the
     * property of the {@link Book}.
     */
    @Test
    @Parameters(method = "getSettableValues")
    public void setBookGenreShouldResultInAPropertyChange(TestParameter parameter) {
        // Act
        book.setGenre(parameter.caseValue);

        // Assert
        assertThat(book).hasGenre(parameter.caseValue);
    }

    /**
     * Tests that setting the year of a book actually changes the
     * property of the {@link Book}.
     */
    @Test
    public void setBookYearShouldResultInAPropertyChange() {
        // Act
        String year = "1978";
        book.setYear(year);

        // Assert
        assertThat(book).hasYear(year);
    }

    /**
     * Tests that setting the authors of a book actually changes the
     * property of the {@link Book}.
     */
    @Test
    public void setBookAuthorsShouldResultInAPropertyChange() {
        // Act (set with empty list)
        List<String> testAuthors = new ArrayList<>();
        book.setAuthors(testAuthors);

        // Assert authors are set properly with empty list
        assertThat(book).hasAuthors(testAuthors);

        // Act (set with populated list)
        testAuthors.add("Some author");
        book.setAuthors(testAuthors);

        // Assert authors are set properly with populated list
        assertThat(book).hasAuthors(testAuthors);
    }

    /**
     * Tests that setting the format of a book actually changes the
     * property of the {@link Book}.
     */
    @Test
    @Parameters(method = "getSettableValues")
    public void setBookFormatShouldResultInAPropertyChange(TestParameter parameter) {
        // Act
        book.setFormat(parameter.caseValue);

        // Assert
        assertThat(book).hasFormat(parameter.caseValue);
    }
}