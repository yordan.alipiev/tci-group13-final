package services;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.jsoup.nodes.Element;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.junit.rules.Timeout;
import org.junit.runner.RunWith;

import java.io.IOException;
import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;

/**
 * Tests the functionality of the {@link JSoupService} class.
 */
@RunWith(JUnitParamsRunner.class)
public class JSoupServiceTest {
    private JSoupService jSoupService = new JSoupService();
    private static final String baseUrl = "https://i339805.hera.fhict.nl/";
    private static final String exampleBookUrl = "details.php?id=102";
    private static final String mediaDetailsClassName = "media-details";

    @Rule
    public final TestRule globalTimeout = Timeout.seconds(5);

    /**
     * Facilitates the passing of string parameters to the tests.
     */
    static class TestParameter {
        public String categoryUrl;
        public String[] subUrls;

        /**
         * Instantiates a test parameter.
         *
         * @param categoryUrl The URL to one of the categories i.e. books, movies, music.
         * @param subUrls     The sub URLs which can be found in the category URL.
         */
        public TestParameter(String categoryUrl, String[] subUrls) {
            this.categoryUrl = categoryUrl;
            this.subUrls = subUrls;
        }
    }

    /**
     * Gets all category URLs that can be found in the website and the sub-URLs which can be found in them.
     *
     * @return A collection of {@link TestParameter} objects.
     */
    private static final TestParameter[] getUrlAndSubUrls() {
        return new TestParameter[]{
                new TestParameter(
                        "catalog.php?cat=books",
                        new String[]{
                                "details.php?id=101",
                                "details.php?id=102",
                                "details.php?id=103",
                                "details.php?id=104"}),
                new TestParameter(
                        "catalog.php?cat=movies",
                        new String[]{
                                "details.php?id=201",
                                "details.php?id=203",
                                "details.php?id=202",
                                "details.php?id=204"}),
                new TestParameter(
                        "catalog.php?cat=music",
                        new String[]{
                                "details.php?id=301",
                                "details.php?id=302",
                                "details.php?id=303",
                                "details.php?id=304"})

        };
    }

    /**
     * Gets the URLs to each category in the website i.e books, movies and music.
     *
     * @return An array of strings.
     */
    private static final String[] getCategoryUrls() {
        return new String[]{
                "catalog.php?cat=books",
                "catalog.php?cat=movies",
                "catalog.php?cat=music",
        };
    }

    /**
     * Tests that {@link JSoupService} successfully retrieves an {@link Element} from a URL.
     */
    @Test
    public void getElementFromUrlWithClassSuccessfully() throws IOException {
        // Arrange


        // Act
        Element element = this.jSoupService.getElementFromUrlWithClass(baseUrl + exampleBookUrl, mediaDetailsClassName);

        // Assert
        assertThat("The retrieved element CSS class name is not correct.", element.className(), is(mediaDetailsClassName));
    }

    /**
     * Tests that {@link JSoupService} successfully retrieves all elements from a URL.
     *
     * @param url The category URL which should be appended to the base URL for testing.
     */
    @Test
    @Parameters(method = "getCategoryUrls")
    public void getElementsFromUrlSuccessfully(String url) throws IOException {
        // Act
        List<Element> elements = this.jSoupService.getElementsFromUrl(baseUrl + url);

        // Assert
        assertThat(
                "The expected elements were not retrieved.",
                elements,
                hasSize(4)); // There are always four elements expected, because of the website structure.
        for (Element e : elements) {
            assertThat("The retrieved element was not expected.", e.className(), is(mediaDetailsClassName));
        }
    }

    /**
     * Tests that {@link JSoupService} successfully retrieves sub-URLs from a category URL.
     *
     * @param testParameter The test parameter to use.
     */
    @Test
    @Parameters(method = "getUrlAndSubUrls")
    public void getUrlsFromUrlSuccessfully(TestParameter testParameter) throws IOException {
        // Act
        List<String> actualUrls = this.jSoupService.getUrlsFromUrl(baseUrl + testParameter.categoryUrl);

        // Assert
        assertThat("Sub-URLs were not retrieved correctly.", actualUrls, hasSize(testParameter.subUrls.length));
        assertThat("Sub-URLs were not retrieved correctly.", actualUrls, containsInAnyOrder(testParameter.subUrls));
    }
}