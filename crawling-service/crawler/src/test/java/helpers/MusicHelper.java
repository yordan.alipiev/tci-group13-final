package helpers;

import models.Music;
import org.jsoup.nodes.Element;

/**
 * Helper class for creating and parsing objects of type {@link Music}.
 */
public class MusicHelper {

    private static String GENRE = "Rock";
    private static String FORMAT = "Vinyl";
    private static String YEAR = "2015";
    private static String ARTIST = "Elvis Presley";

    /**
     * Creates music table element and fills in the fields.
     *
     * @return Element which has a table of type Music.
     */
    public Element createMusicTableElement(String name) {
        Element musicElement = new Element("div");
        musicElement.addClass("media-details");
        musicElement.appendElement("h1").text(name);

        Element tbody = musicElement.appendElement("table").appendElement("tbody");
        Element row = tbody.appendElement("tr");
        row.appendElement("th").text("Category");
        row.appendElement("td").text("Music");

        row = tbody.appendElement("tr");
        row.appendElement("th").text("Genre");
        row.appendElement("td").text(GENRE);

        row = tbody.appendElement("tr");
        row.appendElement("th").text("Format");
        row.appendElement("td").text(FORMAT);

        row = tbody.appendElement("tr");
        row.appendElement("th").text("Year");
        row.appendElement("td").text(YEAR);

        row = tbody.appendElement("tr");
        row.appendElement("th").text("Artist");
        row.appendElement("td").text(ARTIST);

        return musicElement;
    }

    /**
     * Creates an object of type Music and fills in the properties.
     *
     * @return Creates object of type Music with filled properties.
     */
    public Music getParsedMusic(String name) {
        Music parsed = new Music();

        parsed.setFormat(FORMAT);
        parsed.setArtist(ARTIST);
        parsed.setGenre(GENRE);
        parsed.setYear(YEAR);
        parsed.setName(name);

        return parsed;
    }

}
