package exceptions;

/**
 * Exception that might occur when trying to retrieve an object
 * which does not exist.
 */
public class DoesNotExistException extends Exception {

    /**
     * Instantiates a new {@link DoesNotExistException}.
     *
     * @param message The message of the exception.
     */
    public DoesNotExistException(String message) {
        super(message);
    }
}
