package models;

/**
 * Helper class facilitating passing of primitive integer type
 * as reference.
 */
public class MutableInteger {
    private int value;

    /**
     * Initializes the class with integer value of 0.
     */
    public MutableInteger() {
        value = 0;
    }

    /**
     * Increments the integer value contained in the mutable class.
     */
    public void increment() {
        value = value + 1;
    }

    /**
     * Gets the value of the class.
     *
     * @return The current value contained in the class.
     */
    public int getValue() {
        return value;
    }
}
