package custom.matchers;

import models.Book;
import org.fest.assertions.*;

import java.util.List;

/**
 * A custom matcher class providing functionality to assert properties of a
 * {@link Book} object.
 */
public class BookAssert extends GenericAssert<BookAssert, Book> {

    public BookAssert(Book actual) {
        super(BookAssert.class, actual);
    }

    public static BookAssert assertThat(Book actual) {
        return new BookAssert(actual);
    }

    public BookAssert hasName(String name) {
        isNotNull();
        String errorMessage = String.format(
                "Expected book's name to be <%s> but was <%s>",
                name, actual.getName());
        Assertions.assertThat(actual.getName())
                .overridingErrorMessage(errorMessage)
                .isEqualTo(name);
        return this;
    }

    public BookAssert hasGenre(String genre) {
        isNotNull();
        String errorMessage = String.format(
                "Expected book's genre to be <%s> but was <%s>",
                genre, actual.getGenre());
        Assertions.assertThat(actual.getGenre())
                .overridingErrorMessage(errorMessage)
                .isEqualTo(genre);
        return this;
    }

    public BookAssert hasYear(String year) {
        isNotNull();
        String errorMessage = String.format(
                "Expected book's year to be <%s> but was <%s>",
                year, actual.getYear());
        Assertions.assertThat(actual.getYear())
                .overridingErrorMessage(errorMessage)
                .isEqualTo(year);
        return this;
    }

    public BookAssert hasPublisher(String publisher) {
        isNotNull();
        String errorMessage = String.format(
                "Expected book's publisher to be <%s> but was <%s>",
                publisher, actual.getPublisher());
        Assertions.assertThat(actual.getPublisher())
                .overridingErrorMessage(errorMessage)
                .isEqualTo(publisher);
        return this;
    }

    public BookAssert hasISBN(String ISBN) {
        isNotNull();
        String errorMessage = String.format(
                "Expected book's ISBN to be <%s> but was <%s>",
                ISBN, actual.getIsbn());
        Assertions.assertThat(actual.getIsbn())
                .overridingErrorMessage(errorMessage)
                .isEqualTo(ISBN);
        return this;
    }

    public BookAssert hasFormat(String format) {
        isNotNull();
        String errorMessage = String.format(
                "Expected book's format to be <%s> but was <%s>",
                format, actual.getFormat());
        Assertions.assertThat(actual.getFormat())
                .overridingErrorMessage(errorMessage)
                .isEqualTo(format);
        return this;
    }

    public BookAssert hasAuthors(List<String> authors) {
        isNotNull();
        String errorMessage = String.format(
                "Expected book's authors to be <%s> but was <%s>",
                authors, actual.getAuthors());
        Assertions.assertThat(actual.getAuthors())
                .overridingErrorMessage(errorMessage)
                .isEqualTo(authors);
        return this;
    }
}