package services;

import exceptions.ParsingException;
import helpers.BookHelper;
import models.Book;
import models.MutableInteger;
import org.jsoup.nodes.Element;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.junit.rules.Timeout;
import parsers.BookParser;

import java.sql.Time;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;

/**
 * Tests the functionality of the {@link BookService} class.
 */
public class BookServiceTest {
    private BookService bookService;
    private BookParser bookParserMock = mock(BookParser.class);
    private BookHelper bookHelper = new BookHelper();

    @Rule
    public final TestRule globalTimeout = Timeout.seconds(5);

    @Before
    public void setUp() {
        this.bookService = new BookService(bookParserMock);
    }

    /**
     * Tests a happy scenario of retrieving all books from a collection of {@link Element}
     * objects.
     */
    @Test
    public void getAllBooksSuccessfully() throws ParsingException {
        // Arrange
        List<Element> bookElements = new ArrayList<>();
        Element bookElement = this.bookHelper.getBookElement("Book");
        bookElements.add(bookElement);
        bookElements.add(bookElement);
        bookElements.add(bookElement);

        List<Book> expectedBooks = new ArrayList<>();
        Book expected = this.bookHelper.getParsedBook("Book");
        expectedBooks.add(expected);
        expectedBooks.add(expected);
        expectedBooks.add(expected);
        when(this.bookParserMock.parse(bookElement)).thenReturn(expected);

        // Act
        List<Book> actual = this.bookService.getAllBooks(bookElements, new MutableInteger());

        // Assert
        assertThat(
                "The actual size of retrieved books does not match the expected.",
                actual.size(),
                is(expectedBooks.size()));
        assertThat("The retrieved books do not match the expected.",
                actual,
                containsInAnyOrder(expectedBooks));
    }

    /**
     * Tests a happy scenario of retrieving a book from an {@link Element} object.
     */
    @Test
    public void getBookSuccessfully() throws ParsingException {
        // Arrange
        String bookName = "The name of the book";
        Element bookElement = this.bookHelper.getBookElement(bookName);
        Book expected = this.bookHelper.getParsedBook(bookName);
        when(this.bookParserMock.parse(bookElement)).thenReturn(expected);

        // Act
        Book actual = this.bookService.getBook(bookElement);

        // Assert
        assertEquals(expected, actual);
        verify(this.bookParserMock).parse(bookElement);
    }

    private static <T> org.hamcrest.Matcher<java.lang.Iterable<? extends T>> containsInAnyOrder(Collection<T> items) {
        return org.hamcrest.collection.IsIterableContainingInAnyOrder.containsInAnyOrder((T[]) items.toArray());
    }

    /**
     * Tests the functionality of the {@link BookService} to search for a {@link Book} in a list of {@link Element} by providing a property.
     */
    @Test
    public void searchBookSuccessfully() throws ParsingException {
        // Arrange
        List<Element> elements = new ArrayList<>();
        Element bookElement = this.bookHelper.getBookElement("Book name");
        Element randomElement = this.bookHelper.getBookElement("Random name");
        Book expected = this.bookHelper.getParsedBook("Book name");
        elements.add(randomElement);
        elements.add(bookElement);
        when(this.bookParserMock.parseByMatchingProperty(bookElement, "Book name")).thenReturn(expected);

        // Act
        Book actual = this.bookService.searchBook(elements, "Book name", new MutableInteger());
        // Assert
        assertEquals("The expected book was not the same as the actual.", expected, actual);
        verify(this.bookParserMock).parseByMatchingProperty(bookElement, "Book name");
    }

    /**
     * Tests the functionality of the {@link BookService} to search for a {@link Book} in a list of {@link Element} by providing a property which does not exist.
     */
    @Test
    public void searchBookWithNotExistingProperty() throws ParsingException {
        // Arrange
        List<Element> elements = new ArrayList<>();
        Element bookElement = this.bookHelper.getBookElement("Book name");
        elements.add(bookElement);
        bookService = new BookService();

        // Act
        Book actual = this.bookService.searchBook(elements, "Harry Potter", new MutableInteger());

        // Assert
        assertEquals("The expected book was not the same as the actual.", null, actual);
    }
}