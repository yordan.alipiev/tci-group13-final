package services;

import models.*;

/**
 * Provides functionality for transferring information between music, movie
 * and book services and crawler.
 */
public class ScrapingService {

    /**
     * Used for communicating to {@link MusicService}
     */
    private MusicService musicService;

    /**
     * Used for communicating to {@link MovieService}
     */
    private MovieService movieService;

    /**
     * Used for communicating to {@link BookService}
     */
    private BookService bookService;

    /**
     * Initializes object of type ScrapingService.
     *
     * @param bookService  used to set an instance of {@link BookService}.
     * @param movieService used to set an instance of {@link MovieService}.
     * @param musicService used to set an instance of {@link MusicService}.
     */
    public ScrapingService(
            BookService bookService,
            MovieService movieService,
            MusicService musicService) {
        this.bookService = bookService;
        this.movieService = movieService;
        this.musicService = musicService;
    }

    /**
     * Gets an instance of {@link MusicService} which can be used for parsing
     * {@link Music} objects.
     *
     * @return Returns a {@link MusicService}.
     */
    public MusicService getMusicService() {
        return musicService;
    }

    /**
     * Gets an instance of {@link MovieService} which can be used for parsing
     * {@link Movie} objects.
     *
     * @return Returns a {@link MovieService}.
     */
    public MovieService getMovieService() {
        return movieService;
    }

    /**
     * Gets an instance of {@link BookService} which can be used for parsing
     * {@link Book} objects.
     *
     * @return Returns a {@link BookService}.
     */
    public BookService getBookService() {
        return bookService;
    }
}
