package parsers;

import exceptions.ParsingException;
import models.Music;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 * MusicParser implements the GenericParser. Exposes method for parsing
 * a Music object from {@link Element}.
 */
public class MusicParser implements IGenericParser<Music> {

    /**
     * Parses an {@link Element} into a single {@link Music} object.
     *
     * @param element The element to parse.
     * @return Returns parsed Music object.
     * @throws ParsingException in case a property of the music was null.
     */
    @Override
    public Music parse(Element element) throws ParsingException {
        Music music = new Music();

        // If the class name does not match, then the element does not contain
        // the correct data to parse.
        if (!element.className().equals("media-details")) {
            throw new ParsingException();
        } else {
            try {
                String musicName = element.getElementsByTag("h1")
                        .first().text();
                music.setName(musicName);

                // We can safely assume there is only one <tbody> tag, because
                // of the site structure.
                Element tbody = element.getElementsByTag("tbody").first();
                Elements rows = tbody.select("tr");

                for (Element row : rows) {
                    String headerText = row.getElementsByTag("th")
                            .first().text();

                    switch (headerText) {
                        case "Genre":
                            String genre = row.getElementsByTag("td")
                                    .first().text();
                            music.setGenre(genre);
                            break;
                        case "Year":
                            String year = row.getElementsByTag("td")
                                    .first().text();
                            music.setYear(year);
                            break;
                        case "Artist":
                            String publisher = row.getElementsByTag("td")
                                    .first().text();
                            music.setArtist(publisher);
                            break;
                        case "Format":
                            String format = row.getElementsByTag("td")
                                    .first().text();
                            music.setFormat(format);
                            break;
                        case "Category":
                            break;
                        default:
                            throw new ParsingException(
                                    "Unknown property of music encountered.");
                    }
                }
            } catch (Exception e) {
                throw new ParsingException(
                        "An expected tag was missing from the element.");
            }
        }
        return music;
    }

    /**
     * Parses an {@link Element} into a single {@link Music} object and looks
     * for a property matching the provided one.
     *
     * @param element  The element to parse.
     * @param property The property to look for.
     * @return Returns a music if a matching property is found, else null.
     * @throws ParsingException in case a property of the music was null.
     */
    @Override
    public Music parseByMatchingProperty(Element element, String property)
            throws ParsingException {
        Music parsedMusic = this.parse(element);
        property = property.trim();

        if (parsedMusic.getName().contains(property) ||
                parsedMusic.getFormat().contains(property) ||
                parsedMusic.getGenre().contains(property) ||
                parsedMusic.getArtist().contains(property) ||
                parsedMusic.getYear().contains(property)) {
            return parsedMusic;
        }

        return null;
    }
}
