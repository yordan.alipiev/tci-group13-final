package services;

import exceptions.ParsingException;
import helpers.MovieHelper;
import models.Movie;
import models.MutableInteger;
import org.jsoup.nodes.Element;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.junit.rules.Timeout;
import parsers.MovieParser;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;

/**
 * Tests the functionality of the {@link MovieService} class.
 */
public class MovieServiceTest {

    private MovieService sut;
    private MovieParser movieMockParser = mock(MovieParser.class);
    private MovieHelper movieHelper = new MovieHelper();

    @Rule
    public final TestRule globalTimeout = Timeout.seconds(5);

    @Before
    public void setUp() {
        this.sut = new MovieService(movieMockParser);
    }


    /**
     * Tests a happy scenario of retrieving all movies from a collection of
     * {@link Element} objects.
     *
     * @throws ParsingException
     */
    @Test
    public void getAllMoviesSuccessfully() throws ParsingException {
        // Arrange
        ArrayList<Element> movieElements = new ArrayList<>();

        Element movieElement = movieHelper.createMovieTableForElement("LOTR");

        movieElements.add(movieElement);
        movieElements.add(movieElement);
        movieElements.add(movieElement);

        Movie expectedMovie = movieHelper.getParsedMovie("LOTR");

        ArrayList<Movie> expectedMovies = new ArrayList<>();
        expectedMovies.add(expectedMovie);
        expectedMovies.add(expectedMovie);
        expectedMovies.add(expectedMovie);

        when(movieMockParser.parse(movieElement)).thenReturn(expectedMovie);

        // Act
        List<Movie> actual = this.sut.getAllMovies(movieElements, new MutableInteger());

        // Assert
        assertThat("The actual size of retrieved movies does not match the expected.", actual.size(),
                is(expectedMovies.size()));
        assertThat("The retrieved movies do not match the expected.", actual, is(expectedMovies));
    }

    /**
     * Tests a happy scenario of retrieving a movie from a {@link Element} object.
     *
     * @throws ParsingException
     */
    @Test
    public void getMovieSuccessfully() throws ParsingException {
        // Arrange
        Element movieElement = movieHelper.createMovieTableForElement("LOTR");
        Movie expectedMovie = movieHelper.getParsedMovie("LOTR");
        when(this.movieMockParser.parse(movieElement)).thenReturn(expectedMovie);
        // Act
        Movie actual = sut.getMovie(movieElement);

        // Assert
        assertThat(actual, is(expectedMovie));
        verify(this.movieMockParser).parse(movieElement);
    }

    /**
     * Tests the functionality of the {@link MovieService} to search for a {@link Movie} in a list of {@link Element} by providing a property.
     */
    @Test
    public void searchMovieSuccessfully() throws ParsingException {
        // Arrange
        List<Element> elements = new ArrayList<>();
        Element movieElement = this.movieHelper.createMovieTableForElement("LOTR");
        Element randomElement = this.movieHelper.createMovieTableForElement("Narnia");
        Movie expected = this.movieHelper.getParsedMovie("LOTR");
        elements.add(randomElement);
        elements.add(movieElement);
        when(this.movieMockParser.parseByMatchingProperty(movieElement, "LOTR")).thenReturn(expected);

        // Act
        Movie actual = this.sut.searchMovie(elements, "LOTR", new MutableInteger());
        // Assert
        Assert.assertEquals("The expected movie was not the same as the actual.", expected, actual);
        verify(this.movieMockParser).parseByMatchingProperty(movieElement, "LOTR");
    }

    /**
     * Tests the functionality of the {@link MovieService} to search for a {@link Movie} in a list of {@link Element} by providing a property which does not exist.
     */
    @Test
    public void searchMovieWithNotExistingProperty() throws ParsingException {
        List<Element> elements = new ArrayList<>();
        Element movieElement = this.movieHelper.createMovieTableForElement("LOTR");
        elements.add(movieElement);
        sut = new MovieService();

        // Act
        Movie actual = this.sut.searchMovie(elements, "Narnia", new MutableInteger());

        // Assert
        Assert.assertEquals("The expected movie was not the same as the actual.", null, actual);
    }
}