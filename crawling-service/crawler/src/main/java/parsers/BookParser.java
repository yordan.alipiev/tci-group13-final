package parsers;

import exceptions.ParsingException;
import models.Book;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.Arrays;
import java.util.List;

/**
 * Parser for book elements.
 */
public class BookParser implements IGenericParser<Book> {

    /**
     * Parses an {@link Element} into a single {@link Book} object.
     *
     * @param element The element to parse.
     * @return Returns a {@link Book} object.
     * @throws ParsingException in case a property of the book was null.
     */
    @Override
    public Book parse(Element element) throws ParsingException {
        Book book = new Book();

        // If the class name does not match, then the element does not contain
        // the correct data to parse.
        if (!element.className().equals("media-details")) {
            throw new ParsingException();
        } else {
            try {
                String bookName = element.getElementsByTag("h1").first().text();
                book.setName(bookName);

                // We can safely assume there is only one <tbody> tag, because
                // of the site structure.
                Element tbody = element.getElementsByTag("tbody").first();
                Elements rows = tbody.select("tr");

                for (Element row : rows) {
                    String headerText = row.getElementsByTag("th")
                            .first().text();

                    switch (headerText) {
                        case "Genre":
                            String genre = row.getElementsByTag("td")
                                    .first().text();
                            book.setGenre(genre);
                            break;
                        case "Year":
                            String year = row.getElementsByTag("td")
                                    .first().text();
                            book.setYear(year);
                            break;
                        case "Publisher":
                            String publisher = row.getElementsByTag("td")
                                    .first().text();
                            book.setPublisher(publisher);
                            break;
                        case "ISBN":
                            String isbn = row.getElementsByTag("td")
                                    .first().text();
                            book.setIsbn(isbn);
                            break;
                        case "Format":
                            String format = row.getElementsByTag("td")
                                    .first().text();
                            book.setFormat(format);
                            break;
                        case "Authors":
                            List<String> authors = this.splitString(
                                    row.getElementsByTag("td").first().text());
                            book.setAuthors(authors);
                            break;
                        case "Category":
                            break;
                        default:
                            throw new ParsingException(
                                    "Unknown property of book encountered.");
                    }
                }
            } catch (Exception e) {
                throw new ParsingException(
                        "An expected tag was missing from the element.");
            }
        }

        return book;
    }

    /**
     * Parses an {@link Element} into a single {@link Book} object and looks
     * for a property matching the provided one.
     *
     * @param element  The element to parse.
     * @param property The property to look for.
     * @return Returns a book if a matching property is found, else null.
     * @throws ParsingException in case a property of the book was null.
     */
    @Override
    public Book parseByMatchingProperty(Element element, String property)
            throws ParsingException {
        Book parsedBook = this.parse(element);
        property = property.trim();

        if (parsedBook.getName().contains(property) ||
                parsedBook.getFormat().contains(property) ||
                parsedBook.getGenre().contains(property) ||
                parsedBook.getIsbn().contains(property) ||
                parsedBook.getPublisher().contains(property) ||
                parsedBook.getYear().contains(property) ||
                parsedBook.getAuthors().contains(property)) {
            return parsedBook;
        }

        return null;
    }

    /**
     * Splits a string containing multiple elements separated by a
     * comma and puts them in a collection.
     *
     * @param string The string to split.
     * @return A collection of strings.
     */
    private List<String> splitString(String string) {
        return Arrays.asList(string.split(", "));
    }
}
