package services;

import exceptions.DoesNotExistException;
import exceptions.InvalidBaseAddressException;
import exceptions.ParsingException;
import helpers.BookHelper;
import helpers.MovieHelper;
import helpers.MusicHelper;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import models.*;
import org.jsoup.nodes.Element;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.junit.rules.Timeout;
import org.junit.runner.RunWith;
import org.mockito.Matchers;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Tests the functionality of the {@link CrawlingService} class.
 */
@RunWith(JUnitParamsRunner.class)
public class CrawlingServiceTest {

    private CrawlingService sut;
    private ScrapingService scrapingServiceMock;
    private JSoupService jSoupServiceMock = mock(JSoupService.class);
    private BookService bookServiceMock = mock(BookService.class);
    private MovieService movieServiceMock = mock(MovieService.class);
    private MusicService musicServiceMock = mock(MusicService.class);
    private static BookHelper bookHelper = new BookHelper();
    private static MusicHelper musicHelper = new MusicHelper();
    private static MovieHelper movieHelper = new MovieHelper();
    private static final String baseAddress = "https://i339805.hera.fhict.nl/";

    @Rule
    public final TestRule globalTimeout = Timeout.seconds(5);

    /**
     * Executed every time before a test.
     */
    @Before
    public void setUp() throws NoSuchFieldException, IllegalAccessException {
        // Arrange
        this.scrapingServiceMock = new ScrapingService(bookServiceMock, movieServiceMock, musicServiceMock);
        this.sut = new CrawlingService();

        // Set up the inner private fields using a reflection.
        Class sutClass = sut.getClass();

        // set up the jsoupService
        Field jSoupField = sutClass.getDeclaredField("jSoupService");
        jSoupField.setAccessible(true);
        jSoupField.set(sut, jSoupServiceMock);
        // set up the scrapingService
        Field scrapingField = sutClass.getDeclaredField("scrapingService");
        scrapingField.setAccessible(true);
        scrapingField.set(sut, scrapingServiceMock);
    }

    /**
     * Facilitates the passing of test parameters to the tests.
     */
    static class TestParameter {
        public String type;
        public String property;
        public Element element;
        public Book expectedBook;
        public Movie expectedMovie;
        public Music expectedMusic;
        public String url;

        /**
         * Creates a test parameter object containing two strings.
         */
        public TestParameter(String type, String property) {
            this.type = type;
            this.property = property;
        }

        /**
         * Creates a test parameter object containing two strings.
         */
        public TestParameter(String type, String property, Element element, Book book, Music music, Movie movie, String url) {
            this.type = type;
            this.property = property;
            this.element = element;
            this.expectedBook = book;
            this.expectedMovie = movie;
            this.expectedMusic = music;
            this.url = url;
        }
    }

    /**
     * Gets a list of the test parameters with dummy properties that would cause
     * {@link DoesNotExistException} when used for retrieving a book,
     * movie or music.
     */
    private static final TestParameter[] getNonExistentProperties() {
        return new TestParameter[]{
                new TestParameter("book", "property which doesn't exist"),
                new TestParameter("movie", "property which doesn't exist"),
                new TestParameter("music", "property which doesn't exist"),
        };
    }

    /**
     * Gets a list of the test parameters with existing properties that would cause
     * {@link DoesNotExistException} when used for retrieving a book,
     * movie or music.
     */
    private static final TestParameter[] getExistingProperties() {
        return new TestParameter[]{
                new TestParameter(
                        "book",
                        "Hardcover",
                        bookHelper.getBookElement("Refactoring: Improving the Design of Existing Code"),
                        bookHelper.getParsedBook("Refactoring: Improving the Design of Existing Code"),
                        null,
                        null,
                        "catalog.php?cat=books"),
                new TestParameter(
                        "music",
                        "Vinyl",
                        musicHelper.createMusicTableElement("Elvis Forever"),
                        null,
                        musicHelper.getParsedMusic("Elvis Forever"),
                        null,
                        "catalog.php?cat=music"),
                new TestParameter(
                        "movie",
                        "Lord of the Rings",
                        movieHelper.createMovieTableForElement("LOTR"),
                        null,
                        null,
                        movieHelper.getParsedMovie("LOTR"),
                        "catalog.php?cat=movies"),
        };
    }

    /**
     * Tests that all site data from a given base address is retrieved correctly.
     */
    @Test
    public void getAllSiteDataFromAnAddressCorrectly() throws IOException, ParsingException, InvalidBaseAddressException {
        // Arrange
        List<Book> expectedBooks = Arrays.asList(bookHelper.getParsedBook("Refactoring: Improving the Design of Existing Code"));
        List<Music> expectedMusic = Arrays.asList(musicHelper.getParsedMusic("Elvis Forever"));
        List<Movie> expectedMovies = Arrays.asList(movieHelper.getParsedMovie("LOTR"));

        when(bookServiceMock.getAllBooks(anyListOf(Element.class), any(MutableInteger.class))).thenReturn(expectedBooks);
        when(musicServiceMock.getAllMusic(anyListOf(Element.class), any(MutableInteger.class))).thenReturn(expectedMusic);
        when(movieServiceMock.getAllMovies(anyListOf(Element.class), any(MutableInteger.class))).thenReturn(expectedMovies);

        // Act
        SiteData siteData = this.sut.getAllSiteData(baseAddress);

        // Assert
        assertThat("All books were not retrieved correctly.", siteData.getBooks(), is(expectedBooks));
        assertThat("All music was not retrieved correctly.", siteData.getMusic(), is(expectedMusic));
        assertThat("All movies were not retrieved correctly.", siteData.getMovies(), is(expectedMovies));
    }

    /**
     * Tests that an {@link InvalidBaseAddressException} is thrown when trying to
     * retrieve all site data from a base address where no action can be determined.
     */
    @Test(expected = InvalidBaseAddressException.class)
    public void getAllSiteDataFromAnInvalidBaseAddressThrowsException() throws InvalidBaseAddressException, IOException, ParsingException {
        // Arrange
        when(bookServiceMock.getAllBooks(Matchers.anyListOf(Element.class), any(MutableInteger.class)))
                .thenThrow(ParsingException.class);
        // Act
        this.sut.getAllSiteData("https://www.google.com/");
    }

    /**
     * Tests that retrieving an item of types book, music and movie, by providing an
     * existing property is retrieved successfully.
     */
    @Test
    @Parameters(method = "getExistingProperties")
    public void getItemWithExistingPropertySuccessfully(TestParameter testParameter) throws InvalidBaseAddressException, IOException, ParsingException, DoesNotExistException {
        // Arrange
        MutableInteger integer = new MutableInteger();
        //integer.increment();

        when(jSoupServiceMock
                .getElementsFromUrl(baseAddress + testParameter.url))
                .thenReturn(Arrays.asList(testParameter.element));
        when(this.bookServiceMock
                .searchBook(anyListOf(Element.class), any(String.class), any(MutableInteger.class)))
                .thenReturn(testParameter.expectedBook);
        when(this.musicServiceMock
                .searchMusic(anyListOf(Element.class), any(String.class), any(MutableInteger.class)))
                .thenReturn(testParameter.expectedMusic);
        when(this.movieServiceMock
                .searchMovie(anyListOf(Element.class), any(String.class), any(MutableInteger.class)))
                .thenReturn(testParameter.expectedMovie);

        // Act
        SiteData sd = sut.getItem(baseAddress, testParameter.type, testParameter.property);

        // Assert
        if (testParameter.expectedBook != null)
            assertThat(sd.getBooks(), is(Arrays.asList(testParameter.expectedBook)));
        if (testParameter.expectedMusic != null)
            assertThat(sd.getMusic(), is(Arrays.asList(testParameter.expectedMusic)));
        if (testParameter.expectedMovie != null)
            assertThat(sd.getMovies(), is(Arrays.asList(testParameter.expectedMovie)));
    }

    /**
     * Tests that an {@link InvalidBaseAddressException} is thrown when trying to
     * retrieve an item from a base address where no action can be determined.
     */
    @Test(expected = InvalidBaseAddressException.class)
    @Parameters(method = "getExistingProperties")
    public void getItemFromAnInvalidBaseAddressThrowsException(TestParameter testParameter) throws InvalidBaseAddressException, IOException, ParsingException, DoesNotExistException {
        // Arrange
        when(bookServiceMock.searchBook(Matchers.anyListOf(Element.class), any(String.class), any(MutableInteger.class)))
                .thenThrow(ParsingException.class);
        when(movieServiceMock.searchMovie(anyListOf(Element.class), any(String.class), any(MutableInteger.class)))
                .thenThrow(ParsingException.class);
        when(musicServiceMock.searchMusic(anyListOf(Element.class), any(String.class), any(MutableInteger.class)))
                .thenThrow(ParsingException.class);
        // Act
        sut.getItem("https://www.google.com/", testParameter.type, testParameter.property);
    }

    /**
     * Tests that trying to retrieve an item with a type which does not exist throws
     * a {@link DoesNotExistException}.
     */
    @Test(expected = DoesNotExistException.class)
    public void getItemWithNonExistentTypeThrowsException() throws InvalidBaseAddressException, IOException, ParsingException, DoesNotExistException {
        // Act
        sut.getItem(baseAddress, "random type", "some property");
    }

    /**
     * Tests that trying to retrieve a book, music or movie with a non existent property
     * throws a {@link DoesNotExistException}.
     */
    @Test
    @Parameters(method = "getNonExistentProperties")
    public void getItemWithNonExistentPropertyReturnsNull(TestParameter testParameter) throws InvalidBaseAddressException, IOException, ParsingException, DoesNotExistException {
        // Arrange
        when(jSoupServiceMock
                .getElementsFromUrl(baseAddress + testParameter.url))
                .thenReturn(Arrays.asList(testParameter.element));
        when(this.bookServiceMock
                .searchBook(anyListOf(Element.class), any(String.class), any(MutableInteger.class)))
                .thenReturn(null);
        when(this.musicServiceMock
                .searchMusic(anyListOf(Element.class), any(String.class), any(MutableInteger.class)))
                .thenReturn(null);
        when(this.movieServiceMock
                .searchMovie(anyListOf(Element.class), any(String.class), any(MutableInteger.class)))
                .thenReturn(null);

        // Act
        SiteData sd = sut.getItem(baseAddress, testParameter.type, testParameter.property);
        // Assert
        if (testParameter.expectedBook != null)
            assertThat(sd.getBooks().get(0), is(isNull()));
        if (testParameter.expectedMusic != null)
            assertThat(sd.getMusic().get(0), is(isNull()));
        if (testParameter.expectedMovie != null)
            assertThat(sd.getMovies().get(0), is(isNull()));
    }
}