package custom.matchers;

import models.Music;
import org.fest.assertions.Assertions;
import org.fest.assertions.GenericAssert;

/**
 * A custom matcher class providing functionality to assert properties of a
 * {@link Music} object.
 */
public class MusicAssert extends GenericAssert<MusicAssert, Music> {

    public MusicAssert(Music actual) {
        super(MusicAssert.class, actual);
    }

    public static MusicAssert assertThat(Music actual) {
        return new MusicAssert(actual);
    }

    public MusicAssert hasName(String name) {
        isNotNull();
        String errorMessage = String.format("Expected music name <%s> but was <%s>", name, actual.getName());
        Assertions.assertThat(actual.getName()).overridingErrorMessage(errorMessage).isEqualTo(name);
        return this;
    }

    public MusicAssert hasGenre(String genre) {
        isNotNull();
        String errorMessage = String.format("Expected genre <%s> but was <%s>", genre, actual.getGenre());
        Assertions.assertThat(actual.getGenre()).overridingErrorMessage(errorMessage).isEqualTo(genre);
        return this;
    }

    public MusicAssert hasFormat(String format) {
        isNotNull();
        String errorMessage = String.format("Expected format <%s> but was <%s>", format, actual.getFormat());
        Assertions.assertThat(actual.getFormat()).overridingErrorMessage(errorMessage).isEqualTo(format);
        return this;
    }

    public MusicAssert hasYear(String year){
        isNotNull();
        String errorMessage = String.format("Expected year <%s> but was <%s>",year,actual.getYear());
        Assertions.assertThat(actual.getYear()).overridingErrorMessage(errorMessage).isEqualTo(year);
        return this;
    }

    public MusicAssert hasArtist(String artist){
        isNotNull();
        String errorMessage = String.format("Expected year <%s> but was <%s>",artist,actual.getArtist());
        Assertions.assertThat(actual.getArtist()).overridingErrorMessage(errorMessage).isEqualTo(artist);
        return this;
    }
}
