package custom.matchers;

import models.Movie;
import org.fest.assertions.*;

import java.util.List;

/**
 * A custom matcher class providing functionality to assert properties of a
 * {@link Movie} object.
 */
public class MovieAssert extends GenericAssert<MovieAssert, Movie> {

    public MovieAssert(Movie actual) {
        super(MovieAssert.class, actual);
    }

    public static MovieAssert assertThat(Movie actual) {
        return new MovieAssert(actual);
    }

    public MovieAssert hasName(String name) {
        isNotNull();
        String errorMessage = String.format(
                "Expected movie's name to be <%s> but was <%s>",
                name, actual.getName());
        Assertions.assertThat(actual.getName())
                .overridingErrorMessage(errorMessage)
                .isEqualTo(name);
        return this;
    }

    public MovieAssert hasGenre(String genre) {
        isNotNull();
        String errorMessage = String.format(
                "Expected movie's genre to be <%s> but was <%s>",
                genre, actual.getGenre());
        Assertions.assertThat(actual.getGenre())
                .overridingErrorMessage(errorMessage)
                .isEqualTo(genre);
        return this;
    }

    public MovieAssert hasYear(String year) {
        isNotNull();
        String errorMessage = String.format(
                "Expected movie's year to be <%s> but was <%s>",
                year, actual.getYear());
        Assertions.assertThat(actual.getYear())
                .overridingErrorMessage(errorMessage)
                .isEqualTo(year);
        return this;
    }

    public MovieAssert hasDirector(String director) {
        isNotNull();
        String errorMessage = String.format(
                "Expected movie's director to be <%s> but was <%s>",
                director, actual.getDirector());
        Assertions.assertThat(actual.getDirector())
                .overridingErrorMessage(errorMessage)
                .isEqualTo(director);
        return this;
    }


    public MovieAssert hasFormat(String format) {
        isNotNull();
        String errorMessage = String.format(
                "Expected movie's format to be <%s> but was <%s>",
                format, actual.getFormat());
        Assertions.assertThat(actual.getFormat())
                .overridingErrorMessage(errorMessage)
                .isEqualTo(format);
        return this;
    }

    public MovieAssert hasStars(List<String> stars) {
        isNotNull();
        String errorMessage = String.format(
                "Expected movie's stars to be <%s> but was <%s>",
                stars, actual.getStars());
        Assertions.assertThat(actual.getStars())
                .overridingErrorMessage(errorMessage)
                .isEqualTo(stars);
        return this;
    }

    public MovieAssert hasWriters(List<String> writers) {
        isNotNull();
        String errorMessage = String.format(
                "Expected movie's writers to be <%s> but was <%s>",
                writers, actual.getWriters());
        Assertions.assertThat(actual.getWriters())
                .overridingErrorMessage(errorMessage)
                .isEqualTo(writers);
        return this;
    }
}