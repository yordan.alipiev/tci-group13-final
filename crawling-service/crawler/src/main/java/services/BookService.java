package services;

import exceptions.ParsingException;
import models.Book;
import models.MutableInteger;
import org.jsoup.nodes.Element;
import parsers.BookParser;

import java.util.ArrayList;
import java.util.List;

/**
 * Service providing functionality to convert {@link Element} objects to
 * {@link Book} objects.
 */
public class BookService {
    private BookParser bookParser;

    /**
     * Initializes a new instance of class {@link BookService}.
     *
     * @param bookParser A parser for creating books from DOM elements.
     */
    public BookService(BookParser bookParser) {
        this.bookParser = bookParser;
    }

    /**
     * Initializes a new instance of class {@link BookService}.
     */
    public BookService() {
        this.bookParser = new BookParser();
    }

    /**
     * Gets a list of books from the provided elements.
     *
     * @param elements The elements which should be parsed to books.
     * @param pages    The number of pages.
     * @return Returns a list of books.
     * @throws ParsingException Thrown by the parser.
     */
    public List<Book> getAllBooks(List<Element> elements, MutableInteger pages)
            throws ParsingException {
        List<Book> parsedBooks = new ArrayList<>();

        for (Element e : elements) {
            pages.increment();
            parsedBooks.add(this.bookParser.parse(e));
        }

        return parsedBooks;
    }

    /**
     * Gets a book from the provided element.
     *
     * @param element The element which should be parsed to a book.
     * @return Returns a book.
     * @throws ParsingException Thrown by the parser.
     */
    public Book getBook(Element element) throws ParsingException {
        return this.bookParser.parse(element);
    }

    /**
     * Searches for a book with a matching property in the list
     * of provided elements.
     *
     * @param elements The elements which should be searched.
     * @param property The property to look for.
     * @param pages    Reference integer indicating the number of
     *                 pages explored.
     * @return Returns a {@link Book} with a matching property or
     * null, if none is found.
     * @throws ParsingException Thrown by the {@link BookParser}.
     */
    public Book searchBook(
            List<Element> elements,
            String property,
            MutableInteger pages) throws ParsingException {
        for (Element e : elements) {
            pages.increment();
            Book b = this.bookParser.parseByMatchingProperty(e, property);
            if (b != null) {
                return b;
            }
        }
        return null;
    }
}
