package services;

import exceptions.DoesNotExistException;
import exceptions.ParsingException;
import models.*;
import exceptions.InvalidBaseAddressException;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * Provides functionality for crawling a website and retrieving its data.
 */
public class CrawlingService {
    private ScrapingService scrapingService;
    private JSoupService jSoupService;
    private static String bookUrl = "catalog.php?cat=books";
    private static String musicUrl = "catalog.php?cat=music";
    private static String movieUrl = "catalog.php?cat=movies";

    /**
     * Initializes a new instance of class {@link CrawlingService}.
     */
    public CrawlingService() {
        this.scrapingService = new ScrapingService(
                new BookService(), new MovieService(), new MusicService());
        this.jSoupService = new JSoupService();
    }

    /**
     * Retrieves all data from the website with the given base address.
     *
     * @param baseAddress The address of the website to retrieve from.
     * @return Returns a {@link SiteData} object.
     * @throws InvalidBaseAddressException in case no action could be determined
     *                                     on the provided address.
     */
    public SiteData getAllSiteData(String baseAddress)
            throws InvalidBaseAddressException {

        SiteData siteData = new SiteData();
        try {
            MutableInteger pagesExplored = new MutableInteger();
            // Increment the explored pages because of the base address.
            pagesExplored.increment();
            // Retrieve all books, movies and music.
            List<Book> allBooks = this.scrapingService.getBookService()
                    .getAllBooks(this.jSoupService.getElementsFromUrl(
                            baseAddress + bookUrl), pagesExplored);

            List<Movie> allMovies = this.scrapingService.getMovieService()
                    .getAllMovies(this.jSoupService.getElementsFromUrl(
                            baseAddress + movieUrl), pagesExplored);

            List<Music> allMusic = this.scrapingService.getMusicService()
                    .getAllMusic(this.jSoupService.getElementsFromUrl(
                            baseAddress + musicUrl), pagesExplored);

            // Set all retrieved items.
            siteData.setBooks(allBooks);
            siteData.setMovies(allMovies);
            siteData.setMusic(allMusic);
            siteData.setPagesExplored(pagesExplored.getValue());
            siteData.setDepth(3);

            return siteData;
        } catch (ParsingException | IOException e) {
            throw new InvalidBaseAddressException(
                    "The provided url: " + baseAddress + " is not correct!");
        }
    }

    /**
     * Retrieves an item of the given type (book, movie or music) with the
     * given property from the website with the given base address.
     *
     * @param baseAddress The address of the website to retrieve from.
     * @param type        The type of item to retrieve. Can be book, movie
     *                   or music.
     * @param property    The property of the item to look for. If type is
     *                   "book", this can be a name of an author, for example.
     * @return Returns a {@link SiteData} object which contains the item which
     * as inquired.
     * @throws InvalidBaseAddressException in case no action could be determined
     *                                     on the provided address.
     * @throws DoesNotExistException       in case an item with the provided
     * type does not exist or an item with the provided property does not exist.
     */
    public SiteData getItem(String baseAddress, String type, String property)
            throws InvalidBaseAddressException, DoesNotExistException {
        try {
            SiteData siteData = new SiteData();
            MutableInteger pagesExplored = new MutableInteger();
            // Increment the explored pages because of the base address.
            pagesExplored.increment();

            switch (type) {
                case "book":
                    Book book = this.scrapingService
                            .getBookService()
                            .searchBook(
                                    this.jSoupService.getElementsFromUrl(
                                            baseAddress + this.bookUrl),
                                    property,
                                    pagesExplored);
                    siteData.setBooks(Arrays.asList(book));
                    break;
                case "music":
                    Music music = this.scrapingService.
                            getMusicService()
                            .searchMusic(
                                    this.jSoupService.getElementsFromUrl(
                                            baseAddress + this.musicUrl),
                                    property,
                                    pagesExplored);
                    siteData.setMusic(Arrays.asList(music));
                    break;
                case "movie":
                    Movie movie = this.scrapingService.
                            getMovieService()
                            .searchMovie(
                                    this.jSoupService.getElementsFromUrl(
                                            baseAddress + this.movieUrl),
                                    property,
                                    pagesExplored);
                    siteData.setMovies(Arrays.asList(movie));
                    break;
                default:
                    throw new DoesNotExistException(
                            "Type with such name " + type + " does not exist.");
            }
            siteData.setPagesExplored(pagesExplored.getValue());
            // In case no exception was thrown the depth of the search is three.
            siteData.setDepth(3);
            return siteData;
        } catch (ParsingException | IOException e) {
            throw new InvalidBaseAddressException(
                    "The provided url: " + baseAddress + " is not correct!");
        }
    }
}
